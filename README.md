## Software
Software
1) Microsoft Visual Studio Community 2022 
2) SQL Server Management Studio 19.1(SSMS) 
3) Microsoft SQL Server 16.0

Framework
1) .NET Core 7.0

## Database Setup
1) Open SSMS, create a new database SleekFlow
2) Execute "Database Setup Script.sql" in source code folder,

## How to Run The Project 
1) Open SleekFlow.sln
2) Change appsettings.json(dotnet\src\ApiGateways\Web.Bff.Admin\appsettings.json)  and point to correct database server
3) Run the project
