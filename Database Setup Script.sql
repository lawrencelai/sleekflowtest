USE [SleekFlow]
GO
/****** Object:  Table [dbo].[Job]    Script Date: 10/29/2023 9:22:52 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Job](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[Name] [nvarchar](30) NOT NULL,
	[Description] [nvarchar](1000) NOT NULL,
	[DueDateUtc] [datetime] NULL,
	[Status] [int] NOT NULL,
	[CreatedBy] [int] NULL,
	[CreatedDateTimeUtc] [datetime] NOT NULL,
	[CompletedDateTimeUtc] [datetime] NULL,
 CONSTRAINT [PK_Task] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[RolePermission]    Script Date: 10/29/2023 9:22:52 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[RolePermission](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[RoleName] [nvarchar](100) NOT NULL,
	[PermissionName] [nvarchar](100) NOT NULL,
 CONSTRAINT [PK_RolePermission] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[User]    Script Date: 10/29/2023 9:22:52 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[User](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[Username] [nvarchar](50) NOT NULL,
	[Code] [nvarchar](50) NULL,
	[Name] [nvarchar](50) NOT NULL,
	[PasswordSalt] [nvarchar](500) NOT NULL,
	[PasswordHash] [nvarchar](500) NOT NULL,
	[LastLoginOnUtc] [datetime] NULL,
	[Active] [bit] NOT NULL,
 CONSTRAINT [PK_User] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[UserRole]    Script Date: 10/29/2023 9:22:52 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[UserRole](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[UserId] [int] NOT NULL,
	[RoleName] [nvarchar](100) NOT NULL,
 CONSTRAINT [PK_UserRole] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
SET IDENTITY_INSERT [dbo].[Job] ON 
GO
INSERT [dbo].[Job] ([Id], [Name], [Description], [DueDateUtc], [Status], [CreatedBy], [CreatedDateTimeUtc], [CompletedDateTimeUtc]) VALUES (1, N'Task1', N'Task1 Desc', NULL, 1, 1, CAST(N'2023-10-29T10:27:43.000' AS DateTime), NULL)
GO
INSERT [dbo].[Job] ([Id], [Name], [Description], [DueDateUtc], [Status], [CreatedBy], [CreatedDateTimeUtc], [CompletedDateTimeUtc]) VALUES (2, N'Task 2', N'Task 2 Edit', NULL, 2, 1, CAST(N'2023-10-29T10:28:50.000' AS DateTime), CAST(N'2023-10-29T13:22:03.000' AS DateTime))
GO
SET IDENTITY_INSERT [dbo].[Job] OFF
GO
SET IDENTITY_INSERT [dbo].[RolePermission] ON 
GO
INSERT [dbo].[RolePermission] ([Id], [RoleName], [PermissionName]) VALUES (1, N'Admin', N'ViewJob')
GO
INSERT [dbo].[RolePermission] ([Id], [RoleName], [PermissionName]) VALUES (2, N'Admin', N'AddJob')
GO
INSERT [dbo].[RolePermission] ([Id], [RoleName], [PermissionName]) VALUES (3, N'Admin', N'EditJob')
GO
SET IDENTITY_INSERT [dbo].[RolePermission] OFF
GO
SET IDENTITY_INSERT [dbo].[User] ON 
GO
INSERT [dbo].[User] ([Id], [Username], [Code], [Name], [PasswordSalt], [PasswordHash], [LastLoginOnUtc], [Active]) VALUES (1, N'user', N'', N'user', N'3XQQ60kGQ7At4ySrJPyGDb', N'bdb41b81ecd96ae70949573933e1def1751451045eb667d81959758e651303870452326257afb6e2ceb15df1e70d77f3a32607cd51d725fdfa0ba577db7bcfd93358515136306b4751374174347953724a5079474462', CAST(N'2023-10-29T21:21:29.000' AS DateTime), 1)
GO
SET IDENTITY_INSERT [dbo].[User] OFF
GO
SET IDENTITY_INSERT [dbo].[UserRole] ON 
GO
INSERT [dbo].[UserRole] ([Id], [UserId], [RoleName]) VALUES (1, 1, N'Admin')
GO
SET IDENTITY_INSERT [dbo].[UserRole] OFF
GO
