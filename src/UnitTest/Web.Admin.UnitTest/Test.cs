using Job.Services;
using Job.Services.Interface;
using Microsoft.Extensions.DependencyInjection;
using System;

namespace Web.Admin.UnitTest
{
    public class Test : BaseUnitTest
    {

        private IJobService _jobService;
        [SetUp]
        public void Setup()
        {
            _jobService = GetService<IJobService>();
        }

        [Test]
        public void IntegrationTest()
        {
            #region Create
            var jobDto = new Job.Dtos.CreateJobDto()
            {
                Id = 1,
                Name = "Name",
                Description = "Description",
                DueDateUtc = DateTime.UtcNow,
                Status = (int)Job.Dtos.Enum.JobStatus.NotStarted,
                CreatedBy = 1,
            };

            var result = _jobService.CreateJob(jobDto).Result;

            //succesfully created
            Assert.IsTrue(result.Id > 0 );
            #endregion

            #region Retrieve
            var job = _jobService.GetJobByIdAsync(result.Id).Result;

            //retrive job successfully
            Assert.IsTrue(job != null);
            #endregion

            #region Update
            job.Name = "Updated Name";
            _jobService.UpdateJobAsync(job);
            job = _jobService.GetJobByIdAsync(result.Id).Result;


            //updated job successfully
            Assert.IsTrue(job.Name == "Updated Name");
            #endregion

            #region Delete
            _jobService.DeleteJobAsync(job.Id);
            System.Threading.Thread.Sleep(3000);
            job = _jobService.GetJobByIdAsync(result.Id).Result;

            Assert.IsTrue(job == null);
            #endregion
        }
    }
}