﻿using FluentMigrator;
using FluentMigrator.Runner;
using FluentMigrator.Runner.Conventions;
using FluentMigrator.Runner.Initialization;
using FluentMigrator.Runner.Processors;
using Job.Services;
using Job.Services.Interface;
using LinqToDB.Expressions;
using LinqToDB.Mapping;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Caching.Memory;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Options;
using Microsoft.Net.Http.Headers;
using Moq;
using Newtonsoft.Json;
using OverCode.Core.Caching;
using OverCode.Core.ComponentModel;
using OverCode.Core.Events;
using OverCode.Core.Infrastructure;
using OverCode.Data;
using OverCode.Data.Attributes;
using OverCode.Data.Mapping;
using OverCode.Data.Migrations;
using OverCode.LinqToDb.Core;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Linq.Expressions;

namespace Web.Admin.UnitTest
{
    public  class BaseUnitTest
    {
        private readonly ServiceProvider _serviceProvider;

        public BaseUnitTest()
        {
            TypeDescriptor.AddAttributes(typeof(List<int>),
                new TypeConverterAttribute(typeof(GenericListTypeConverter<int>)));
            TypeDescriptor.AddAttributes(typeof(List<string>),
                new TypeConverterAttribute(typeof(GenericListTypeConverter<string>)));

            var services = new ServiceCollection();

            var memoryCache = new MemoryCache(new Microsoft.Extensions.Caching.Memory.MemoryCacheOptions());
            var typeFinder = new AppDomainTypeFinder();

            services.Configure<DataSettings>(options =>
            {
                options.ConnectionString = "server=localhost;Database=Job;UID=sa;PWD=password;";
                options.DataProvider = DataProviderType.SqlServer;
            });

            var mAssemblies = typeFinder.FindClassesOfType<AutoReversingMigration>()
                .Select(t => t.Assembly)
                .Distinct()
                .ToArray();

            services.AddSingleton<ITypeFinder>(typeFinder);

            var httpContext = new DefaultHttpContext
            {
                Request = { Headers = { { HeaderNames.Host, "127.0.0.1" } } }
            };

            var httpContextAccessor = new Mock<IHttpContextAccessor>();
            httpContextAccessor.Setup(p => p.HttpContext).Returns(httpContext);

            services.AddSingleton(httpContextAccessor.Object);

            //data layer
            services.AddTransient<IDataProviderManager, DataProviderManager>();
            services.AddTransient(serviceProvider =>
                serviceProvider.GetRequiredService<IDataProviderManager>().DataProvider);

            //repositories
            services.AddTransient(typeof(IRepository<>), typeof(EntityRepository<>));

            services.AddSingleton<IMemoryCache>(memoryCache);
            services.AddSingleton<IStaticCacheManager, MemoryCacheManager>();
            services.AddSingleton<ILocker, MemoryCacheManager>();

            services.AddSingleton(serviceProvider =>
            {
                //we cannot use serviceProvider cause it will resolve IMigrationManager as scoped.
                var scope = serviceProvider.CreateScope();
                var migrationManager = scope.ServiceProvider.GetRequiredService<IMigrationManager>();
                var mappingSchema = new MappingSchema() { MetadataReader = new FluentMigratorMetadataReader(migrationManager) };

                var typeFinder = scope.ServiceProvider.GetService<ITypeFinder>();

                var deserializeMethod = MemberHelper.MethodOf(() => JsonConvert.DeserializeObject(null!, typeof(int)));

                foreach (var propertyInfo in typeFinder.GetAssemblies()
                    .SelectMany(a => a.GetTypes())
                    .SelectMany(t => t.GetProperties().Where(p => p.GetCustomAttributes(typeof(JsonContentAttribute), true).Any())))
                {
                    var inParam = Expression.Parameter(typeof(string));
                    var lambda = Expression.Lambda(
                        Expression.Convert(
                            Expression.Call(null, deserializeMethod, inParam,
                                Expression.Constant(propertyInfo.PropertyType)),
                            propertyInfo.PropertyType),
                        inParam);

                    mappingSchema.SetConvertExpression(typeof(string), propertyInfo.PropertyType, lambda);
                }

                return mappingSchema;
            });

            //services
            services.AddScoped<IJobService, JobService>();

            //event consumers
            services.AddSingleton<IEventPublisher, EventPublisher>();
            var consumers = typeFinder.FindClassesOfType(typeof(IConsumer<>)).ToList();
            foreach (var consumer in consumers)
                foreach (var findInterface in consumer.FindInterfaces((type, criteria) =>
                {
                    var isMatch = type.IsGenericType && ((Type)criteria).IsAssignableFrom(type.GetGenericTypeDefinition());
                    return isMatch;
                }, typeof(IConsumer<>)))
                    services.AddTransient(findInterface, consumer);

            services
                // add common FluentMigrator services
                .AddFluentMigratorCore()
                .AddScoped<IProcessorAccessor, OverCodeProcessorAccessor>()
                // set accessor for the connection string
                .AddScoped<IConnectionStringAccessor>(x => x.GetRequiredService<IOptions<DataSettings>>().Value)
                .AddScoped<IMigrationManager, MigrationManager>()
                .AddSingleton<IConventionSet, OverCodeConventionSet>()
                .ConfigureRunner(rb =>
                    rb.WithVersionTable(new MigrationVersionInfo()).AddSqlServer()
                        // define the assembly containing the migrations
                        .ScanIn(mAssemblies).For.Migrations());

            _serviceProvider = services.BuildServiceProvider();
            var migrationManager = _serviceProvider.GetService<IMigrationManager>();
            
        }


        public T GetService<T>()
        {
            return _serviceProvider.GetRequiredService<T>();
        }
    }
}
