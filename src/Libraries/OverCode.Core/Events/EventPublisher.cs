﻿using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace OverCode.Core.Events
{
    /// <summary>
    /// Represents the event publisher implementation
    /// </summary>
    public partial class EventPublisher : IEventPublisher
    {
        #region Fields

        private readonly IHttpContextAccessor _httpContextAccessor;
        private readonly IServiceProvider _serviceProvider;
        private readonly ILogger<EventPublisher> _logger;

        #endregion

        #region Ctor

        public EventPublisher(
            IHttpContextAccessor httpContextAccessor,
            IServiceProvider serviceProvider,
            ILogger<EventPublisher> logger)
        {
            _httpContextAccessor = httpContextAccessor;
            _serviceProvider = serviceProvider;
            _logger = logger;
        }

        #endregion

        #region Methods

        /// <summary>
        /// Publish event to consumers
        /// </summary>
        /// <typeparam name="TEvent">Type of event</typeparam>
        /// <param name="event">Event object</param>
        /// <returns>A task that represents the asynchronous operation</returns>
        public virtual async Task PublishAsync<TEvent>(TEvent @event)
        {
            var serviceProvider = _httpContextAccessor?.HttpContext?.RequestServices;
            if (serviceProvider != null)
                await ProcessEventAsync(serviceProvider, @event);

            using (var serviceScope = _serviceProvider.CreateScope())
                await ProcessEventAsync(serviceScope.ServiceProvider, @event);
        }

        private async Task ProcessEventAsync<TEvent>(IServiceProvider serviceProvider, TEvent @event)
        {
            //get all event consumers
            var consumers = serviceProvider.GetServices<IConsumer<TEvent>>();

            foreach (var consumer in consumers)
            {
                try
                {
                    //try to handle published event
                    await consumer.HandleEventAsync(@event);
                }
                catch (Exception exception)
                {
                    //log error, we put in to nested try-catch to prevent possible cyclic (if some error occurs)
                    try
                    {
                        _logger.LogError(exception, exception.Message);
                    }
                    catch
                    {
                        // ignored
                    }
                }
            }
        }

        #endregion
    }
}