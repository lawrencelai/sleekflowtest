﻿using MySqlConnector;
using System.Collections.Generic;

namespace OverCode.Core.Caching
{
    public class MySqlDistributeCacheKeyHandler : IDistributeCacheKeyHandler
    {
        private readonly string _connectionString;
        private readonly string _schemaName;
        private readonly string _tableName;

        public MySqlDistributeCacheKeyHandler(string connectionString, string schemaName, string tableName)
        {
            _connectionString = connectionString;
            _schemaName = schemaName;
            _tableName = tableName;
        }

        public IList<string> GetAllKeys()
        {
            var keys = new List<string>();

            using (var connection = new MySqlConnection(_connectionString))
            {
                connection.Open();

                var query = $"SELECT Id FROM {_schemaName}.{_tableName}";
                var cmd = new MySqlCommand(query, connection);

                using (var reader = cmd.ExecuteReader())
                {
                    while (reader.Read())
                        keys.Add(reader.GetString(0));
                }
            }

            return keys;
        }
    }
}