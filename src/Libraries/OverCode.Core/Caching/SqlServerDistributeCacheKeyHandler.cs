﻿using System.Collections.Generic;
using System.Data.SqlClient;

namespace OverCode.Core.Caching
{
    public class SqlServerDistributeCacheKeyHandler : IDistributeCacheKeyHandler
    {
        private readonly string _connectionString;
        private readonly string _schemaName;
        private readonly string _tableName;

        public SqlServerDistributeCacheKeyHandler(string connectionString, string schemaName, string tableName)
        {
            _connectionString = connectionString;
            _schemaName = schemaName;
            _tableName = tableName;
        }

        public IList<string> GetAllKeys()
        {
            var keys = new List<string>();

            using (var connection = new SqlConnection(_connectionString))
            {
                connection.Open();

                var query = $"SELECT Id FROM {_schemaName}.{_tableName}";
                var cmd = new SqlCommand(query, connection);

                using (var reader = cmd.ExecuteReader())
                {
                    while (reader.Read())
                        keys.Add(reader.GetString(0));
                }
            }

            return keys;
        }
    }
}