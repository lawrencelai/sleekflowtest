﻿using System.Collections.Generic;

namespace OverCode.Core.Caching
{
    public interface IDistributeCacheKeyHandler
    {
        IList<string> GetAllKeys();
    }
}