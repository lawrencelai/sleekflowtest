﻿

namespace OverCode.Core.Cryptography
{
    /// <summary>The wanted hash function.</summary>
    public enum HashType : int
    {
        /// <summary>MD5 Hashing</summary>
        MD5,
        /// <summary>SHA1 Hashing</summary>
        SHA1,
        /// <summary>SHA256 Hashing</summary>
        SHA256,
        /// <summary>SHA384 Hashing</summary>
        SHA384,
        /// <summary>SHA512 Hashing</summary>
        SHA512,
        /// <summary>CRC32 Hashing</summary>
        CRC32
    }
}
