﻿using System;
using System.Security.Cryptography;
using System.Text;

namespace OverCode.Core.Cryptography
{
    public sealed class Hash
    {
        #region "Public Methods"

        /// <summary>Generates the hash of a text.</summary>
        /// <param name="encoding"></param>
        /// <param name="strPlain">The text of which to generate a hash of.</param>
        /// <param name="hshType">The hash function to use.</param>
        /// <returns>The hash as a hexadecimal string.</returns>
        public static string GetHash(Encoding encoding, string strPlain, HashType hshType)
        {
            string strRet = null;
            switch (hshType)
            {
                case HashType.CRC32:
                    strRet = GetCRC32(encoding, strPlain);
                    break;
                case HashType.MD5:
                    strRet = ComputeHash(new MD5CryptoServiceProvider(), encoding, strPlain);
                    break;
                case HashType.SHA1:
                    strRet = ComputeHash(new SHA1Managed(), encoding, strPlain);
                    break;
                case HashType.SHA256:
                    strRet = ComputeHash(new SHA256Managed(), encoding, strPlain);
                    break;
                case HashType.SHA384:
                    strRet = ComputeHash(new SHA384Managed(), encoding, strPlain);
                    break;
                case HashType.SHA512:
                    strRet = ComputeHash(new SHA512Managed(), encoding, strPlain);
                    break;
                default:
                    strRet = "Invalid HashType";
                    break;
            }
            return strRet;
        }

        /// <summary>Checks a text with a hash.</summary>
        /// <param name="encoding"></param>
        /// <param name="strOriginal">The text to compare the hash against.</param>
        /// <param name="strHash">The hash to compare against.</param>
        /// <param name="hshType">The type of hash.</param>
        /// <returns>True if the hash validates, false otherwise.</returns>
        public static bool CheckHash(Encoding encoding, string strOriginal, string strHash, HashType hshType)
        {
            string strOrigHash = GetHash(encoding, strOriginal, hshType);
            return (strOrigHash == strHash);
        }

        /// <summary>Generates the hash of a text.</summary>
        /// <param name="encoding"></param>
        /// <param name="strPlain">The text of which to generate a hash of.</param>
        /// <param name="hshType">The hash function to use.</param>
        /// <param name="salt">The salt for hash.</param>
        /// <returns>The hash as a hexadecimal string.</returns>
        public static string GetHash(Encoding encoding, string strPlain, HashType hshType, string salt)
        {
            string strRet = null;
            switch (hshType)
            {
                case HashType.CRC32:
                    strRet = "Not implemented with salt"; //to be implement
                    break;
                case HashType.MD5:
                    strRet = ComputeHash(new MD5CryptoServiceProvider(), encoding,  strPlain, salt);
                    break;
                case HashType.SHA1:
                    strRet = ComputeHash(new SHA1Managed(), encoding, strPlain, salt);
                    break;
                case HashType.SHA256:
                    strRet = ComputeHash(new SHA256Managed(), encoding, strPlain, salt);
                    break;
                case HashType.SHA384:
                    strRet = ComputeHash(new SHA384Managed(), encoding, strPlain, salt);
                    break;
                case HashType.SHA512:
                    strRet = ComputeHash(new SHA512Managed(), encoding, strPlain, salt);
                    break;
                default:
                    strRet = "Invalid HashType";
                    break;
            }
            return strRet;
        }

        /// <summary>Checks a text with a hash.</summary>
        /// <param name="encoding"></param>
        /// <param name="strOriginal">The text to compare the hash against.</param>
        /// <param name="strHash">The hash to compare against.</param>
        /// <param name="hshType">The type of hash.</param>
        /// <param name="salt">The salt for hash.</param>
        /// <returns>True if the hash validates, false otherwise.</returns>
        public static bool CheckHash(Encoding encoding, string strOriginal, string strHash, HashType hshType, string salt)
        {
            string strOrigHash = GetHash(encoding, strOriginal, hshType, salt);
            return (strOrigHash == strHash);
        }

        public static string CreateSaltKey(int size)
        {
            //generate a cryptographic random number
            using (var provider = new RNGCryptoServiceProvider())
            {
                var buff = new byte[size];
                provider.GetBytes(buff);

                // Return a Base64 string representation of the random number
                return Convert.ToBase64String(buff);
            }
        }

        #endregion

        #region "Hashers"
        private static string GetCRC32(Encoding encoding, string strPlain)
        {
            var messageBytes = encoding.GetBytes(strPlain);
            var crc32Hash = new Crc32ManagedAlgorithm();

            var hashValue = crc32Hash.ComputeHash(messageBytes);
            return string.Format("{0:x8}", BitConverter.ToUInt32(hashValue, 0));
        }

        private static string ComputeHash(HashAlgorithm hash, Encoding encoding, string plainText)
        {
            var MessageBytes = encoding.GetBytes(plainText);
            var strHex = string.Empty;

            var hashValue = hash.ComputeHash(MessageBytes);
            foreach (byte b in hashValue)
                strHex += String.Format("{0:x2}", b);

            return strHex;
        }

        private static string ComputeHash(HashAlgorithm hash, Encoding encoding, string plainText, string salt)
        {
            // Convert plain text into a byte array.
            var plainTextBytes = encoding.GetBytes(plainText);

            // Convert salt into a byte array.
            var saltBytes = encoding.GetBytes(salt);

            // Allocate array, which will hold plain text and salt.
            var plainTextWithSaltBytes =
                    new byte[plainTextBytes.Length + saltBytes.Length];

            // Copy plain text bytes into resulting array.
            for (int i = 0; i < plainTextBytes.Length; i++)
                plainTextWithSaltBytes[i] = plainTextBytes[i];

            // Append salt bytes to the resulting array.
            for (int i = 0; i < saltBytes.Length; i++)
                plainTextWithSaltBytes[plainTextBytes.Length + i] = saltBytes[i];

            // Compute hash value of our plain text with appended salt.
            var hashBytes = hash.ComputeHash(plainTextWithSaltBytes);

            // Create array which will hold hash and original salt bytes.
            var hashWithSaltBytes = new byte[hashBytes.Length +
                                                saltBytes.Length];

            // Copy hash bytes into resulting array.
            for (int i = 0; i < hashBytes.Length; i++)
                hashWithSaltBytes[i] = hashBytes[i];

            // Append salt bytes to the result.
            for (int i = 0; i < saltBytes.Length; i++)
                hashWithSaltBytes[hashBytes.Length + i] = saltBytes[i];

            // Convert result into a hexadecimal string.
            var strHex = string.Empty;
            foreach (var b in hashWithSaltBytes)
                strHex += String.Format("{0:x2}", b);

            // Return the result.
            return strHex;
        }

        #endregion
    }
}
