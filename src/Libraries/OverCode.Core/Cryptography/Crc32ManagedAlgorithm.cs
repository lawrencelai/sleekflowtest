﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography;
using System.Text;

namespace OverCode.Core.Cryptography
{
    internal class Crc32ManagedAlgorithm : HashAlgorithm
    {

        private const Int32 DefaultHashSizeValue = 32;
        private const UInt32 DefaultPolynomial = 0xedb88320u;

        public Crc32ManagedAlgorithm()
        {
            HashSizeValue = DefaultHashSizeValue;
            Polynomial = DefaultPolynomial;
            InitTable();
            Initialize();
        }

        protected override void HashCore(byte[] array, int ibStart, int cbSize)
        {
            byte lookupId = 0;
            for (Int32 i = ibStart; i <= ibStart + cbSize - 1; i++)
            {
                lookupId = (byte)((Value ^ array[i]) & 0xff);
                Value = LookupTable[lookupId] ^ (Value >> 8);
            }
        }

        protected override byte[] HashFinal()
        {
            HashValue = BitConverter.GetBytes(Value ^ UInt32.MaxValue);

            if ((!BitConverter.IsLittleEndian))
            {
                Array.Reverse(HashValue);
            }

            return HashValue;
        }

        public override void Initialize()
        {
            Value = UInt32.MaxValue;
        }

        protected virtual void InitTable()
        {
            LookupTable = (uint[])Array.CreateInstance(typeof(UInt32), 256);

            UInt32 temp = 0u;
            for (UInt32 i = 0; i <= LookupTable.Length - 1; i++)
            {
                temp = i;
                for (Int32 j = 0; j <= 7; j++)
                {
                    if ((temp & 1) == 1)
                    {
                        temp = (temp >> 1) ^ Polynomial;
                    }
                    else
                    {
                        temp >>= 1;
                    }
                }
                LookupTable[i] = temp;
            }
        }

        protected virtual UInt32 Value { get; set; }

        protected virtual UInt32 Polynomial { get; set; }

        protected virtual UInt32[] LookupTable { get; set; }
    }
}
