﻿using LinqToDB;
using LinqToDB.Data;
using LinqToDB.DataProvider;
using LinqToDB.DataProvider.MySql;
using LinqToDB.Mapping;
using LinqToDB.SqlQuery;
using MySql.Data.MySqlClient;
using OverCode.Core;
using OverCode.Core.Cryptography;
using System;
using System.Data;
using System.Data.Common;
using System.Text;
using System.Threading.Tasks;

namespace OverCode.Data.DataProviders
{
    public class OverCodeMySqlDataProvider : BaseDataProvider, IOverCodeDataProvider
    {
        #region Fields

        private static readonly Lazy<IDataProvider> _dataProvider = new(() => new MySqlDataProvider(ProviderName.MySql), true);

        #endregion

        #region Ctor

        public OverCodeMySqlDataProvider(string connectionString, int commandTimeout,
            MappingSchema mappingSchema) : base(connectionString, commandTimeout, mappingSchema)
        {
        }

        #endregion

        #region Utils

        /// <summary>
        /// Creates the database connection
        /// </summary>
        /// <returns>A task that represents the asynchronous operation</returns>
        protected override async Task<DataConnection> CreateDataConnectionAsync()
        {
            var dataContext = await CreateDataConnectionAsync(LinqToDbDataProvider);

            dataContext.MappingSchema.SetDataType(typeof(Guid), new SqlDataType(DataType.NChar, typeof(Guid), 36));
            dataContext.MappingSchema.SetConvertExpression<string, Guid>(strGuid => new Guid(strGuid));

            return dataContext;
        }

        protected MySqlConnectionStringBuilder GetConnectionStringBuilder()
        {
            return new MySqlConnectionStringBuilder(CurrentConnectionString);
        }

        #endregion

        #region Methods

        /// <summary>
        /// Gets a connection to the database for a current data provider
        /// </summary>
        /// <param name="connectionString">Connection string</param>
        /// <returns>Connection to a database</returns>
        protected override DbConnection GetInternalDbConnection(string connectionString)
        {
            if (string.IsNullOrEmpty(connectionString))
                throw new ArgumentException(nameof(connectionString));

            return new MySqlConnection(connectionString);
        }

        /// <summary>
        /// Get the current identity value
        /// </summary>
        /// <typeparam name="TEntity">Entity type</typeparam>
        /// <returns>
        /// A task that represents the asynchronous operation
        /// The task result contains the integer identity; null if cannot get the result
        /// </returns>
        public virtual async Task<int?> GetTableIdentAsync<TEntity>() where TEntity : BaseEntity
        {
            using var currentConnection = await CreateDataConnectionAsync();
            var tableName = GetEntityDescriptor<TEntity>().TableName;
            var databaseName = currentConnection.Connection.Database;

            //we're using the DbConnection object until linq2db solve this issue https://github.com/linq2db/linq2db/issues/1987
            //with DataContext we could be used KeepConnectionAlive option
            await using var dbConnection = GetInternalDbConnection(CurrentConnectionString);

            dbConnection.StateChange += (sender, e) =>
            {
                try
                {
                    if (e.CurrentState != ConnectionState.Open)
                        return;

                    var connection = (IDbConnection)sender;
                    using var internalCommand = connection.CreateCommand();
                    internalCommand.Connection = connection;
                    internalCommand.CommandText = $"SET @@SESSION.information_schema_stats_expiry = 0;";
                    internalCommand.ExecuteNonQuery();
                }
                //ignoring for older than 8.0 versions MySQL (#1193 Unknown system variable)
                catch (MySqlException ex) when (ex.Number == 1193)
                {
                    //ignore
                }
            };

            await using var command = dbConnection.CreateCommand();
            command.Connection = dbConnection;
            command.CommandText = $"SELECT AUTO_INCREMENT FROM information_schema.TABLES WHERE TABLE_SCHEMA = '{databaseName}' AND TABLE_NAME = '{tableName}'";
            await dbConnection.OpenAsync();

            return Convert.ToInt32((await command.ExecuteScalarAsync()) ?? 1);
        }

        /// <summary>
        /// Set table identity (is supported)
        /// </summary>
        /// <typeparam name="TEntity">Entity type</typeparam>
        /// <param name="ident">Identity value</param>
        /// <returns>A task that represents the asynchronous operation</returns>
        public virtual async Task SetTableIdentAsync<TEntity>(int ident) where TEntity : BaseEntity
        {
            var currentIdent = await GetTableIdentAsync<TEntity>();
            if (!currentIdent.HasValue || ident <= currentIdent.Value)
                return;

            using var currentConnection = await CreateDataConnectionAsync();
            var tableName = GetEntityDescriptor<TEntity>().TableName;

            await currentConnection.ExecuteAsync($"ALTER TABLE `{tableName}` AUTO_INCREMENT = {ident};");
        }

        /// <summary>
        /// Gets the name of a foreign key
        /// </summary>
        /// <param name="foreignTable">Foreign key table</param>
        /// <param name="foreignColumn">Foreign key column name</param>
        /// <param name="primaryTable">Primary table</param>
        /// <param name="primaryColumn">Primary key column name</param>
        /// <returns>Name of a foreign key</returns>
        public virtual string CreateForeignKeyName(string foreignTable, string foreignColumn, string primaryTable, string primaryColumn)
        {
            //mySql support only 64 chars for constraint name
            //that is why we use hash function for create unique name
            //see details on this topic: https://dev.mysql.com/doc/refman/8.0/en/identifier-length.html
            return "FK_" + Hash.GetHash(Encoding.UTF8, $"{foreignTable}_{foreignColumn}_{primaryTable}_{primaryColumn}", HashType.SHA1);
        }

        /// <summary>
        /// Gets the name of an index
        /// </summary>
        /// <param name="targetTable">Target table name</param>
        /// <param name="targetColumn">Target column name</param>
        /// <returns>Name of an index</returns>
        public virtual string GetIndexName(string targetTable, string targetColumn)
        {
            return "IX_" + Hash.GetHash(Encoding.UTF8, $"{targetTable}_{targetColumn}", HashType.SHA1);
        }

        #endregion

        #region Properties

        /// <summary>
        /// MySql data provider
        /// </summary>
        protected override IDataProvider LinqToDbDataProvider => _dataProvider.Value;

        #endregion
    }
}
