﻿using LinqToDB;
using LinqToDB.Common;
using LinqToDB.Data;
using LinqToDB.DataProvider;
using LinqToDB.Mapping;
using LinqToDB.SqlQuery;
using Npgsql;
using OverCode.Core;
using OverCode.LinqToDb.Core;
using OverCode.Data.DataProviders.LinqToDB;
using System;
using System.Data;
using System.Data.Common;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace OverCode.Data.DataProviders
{
    /// <summary>
    /// When using PostgreSqlDataProvider, please execute
    /// CREATE EXTENSION IF NOT EXISTS citext; CREATE EXTENSION IF NOT EXISTS pgcrypto;
    /// </summary>
    public class PostgreSqlDataProvider : BaseDataProvider, IOverCodeDataProvider
    {
        #region Fields

        private static readonly Lazy<IDataProvider> _dataProvider = new(() => new LinqToDBPostgreSQLDataProvider(), true);

        #endregion

        #region Ctor

        #region Ctor

        public PostgreSqlDataProvider(string connectionString, int commandTimeout,
            MappingSchema mappingSchema) : base(connectionString, commandTimeout, mappingSchema)
        {
        }

        #endregion

        #endregion

        #region Utils

        /// <summary>
        /// Creates the database connection by the current data configuration
        /// </summary>
        protected override DataConnection CreateDataConnection()
        {
            var dataContext = CreateDataConnection(LinqToDbDataProvider);
            dataContext.MappingSchema.SetDataType(
                typeof(string),
                new SqlDataType(new DbDataType(typeof(string), "citext")));

            return dataContext;
        }

        /// <summary>
        /// Gets a connection to the database for a current data provider
        /// </summary>
        /// <param name="connectionString">Connection string</param>
        /// <returns>Connection to a database</returns>
        protected override DbConnection GetInternalDbConnection(string connectionString)
        {
            if (string.IsNullOrEmpty(connectionString))
                throw new ArgumentException(nameof(connectionString));

            return new NpgsqlConnection(connectionString);
        }

        /// <summary>
        /// Get the name of the sequence associated with a identity column
        /// </summary>
        /// <param name="dataConnection">A database connection object</param>
        /// <typeparam name="TEntity">Entity type</typeparam>
        /// <returns>Returns the name of the sequence, or NULL if no sequence is associated with the column</returns>
        private string GetSequenceName<TEntity>(DataConnection dataConnection) where TEntity : BaseEntity
        {
            if (dataConnection is null)
                throw new ArgumentNullException(nameof(dataConnection));

            var descriptor = GetEntityDescriptor<TEntity>();

            if (descriptor is null)
                throw new Exception($"Mapped entity descriptor is not found: {typeof(TEntity).Name}");

            var tableName = descriptor.TableName;
            var columnName = descriptor.Columns.FirstOrDefault(x => x.IsIdentity && x.IsPrimaryKey)?.ColumnName;

            if (string.IsNullOrEmpty(columnName))
                throw new Exception("A table's primary key does not have an identity constraint");

            return dataConnection.Query<string>($"SELECT pg_get_serial_sequence('\"{tableName}\"', '{columnName}');")
                .FirstOrDefault();
        }

        #endregion

        #region Methods

        /// <summary>
        /// Get the current identity value
        /// </summary>
        /// <typeparam name="TEntity">Entity type</typeparam>
        /// <returns>
        /// A task that represents the asynchronous operation
        /// The task result contains the integer identity; null if cannot get the result
        /// </returns>
        public virtual async Task<int?> GetTableIdentAsync<TEntity>() where TEntity : BaseEntity
        {
            using var currentConnection = await CreateDataConnectionAsync();

            var seqName = GetSequenceName<TEntity>(currentConnection);

            var result = currentConnection.Query<int>($"SELECT COALESCE(last_value + CASE WHEN is_called THEN 1 ELSE 0 END, 1) as Value FROM {seqName};")
                .FirstOrDefault();

            return result;
        }

        /// <summary>
        /// Set table identity (is supported)
        /// </summary>
        /// <typeparam name="TEntity">Entity type</typeparam>
        /// <param name="ident">Identity value</param>
        /// <returns>A task that represents the asynchronous operation</returns>
        public virtual async Task SetTableIdentAsync<TEntity>(int ident) where TEntity : BaseEntity
        {
            var currentIdent = await GetTableIdentAsync<TEntity>();
            if (!currentIdent.HasValue || ident <= currentIdent.Value)
                return;

            using var currentConnection = await CreateDataConnectionAsync();

            var seqName = GetSequenceName<TEntity>(currentConnection);

            await currentConnection.ExecuteAsync($"select setval('{seqName}', {ident}, false);");
        }

        /// <summary>
        /// Inserts record into table. Returns inserted entity with identity
        /// </summary>
        /// <param name="entity"></param>
        /// <typeparam name="TEntity"></typeparam>
        /// <returns>Inserted entity</returns>
        public override TEntity InsertEntity<TEntity>(TEntity entity)
        {
            using var dataContext = CreateDataConnection();
            try
            {
                entity.Id = dataContext.InsertWithInt32Identity(entity);
            }
            // Ignore when we try insert foreign entity via InsertWithInt32IdentityAsync method
            catch (global::LinqToDB.SqlQuery.SqlException ex) when (ex.Message.StartsWith("Identity field must be defined for"))
            {
                dataContext.Insert(entity);
            }

            return entity;
        }

        /// <summary>
        /// Inserts record into table. Returns inserted entity with identity
        /// </summary>
        /// <param name="entity"></param>
        /// <typeparam name="TEntity"></typeparam>
        /// <returns>
        /// A task that represents the asynchronous operation
        /// The task result contains the inserted entity
        /// </returns>
        public override async Task<TEntity> InsertEntityAsync<TEntity>(TEntity entity)
        {
            using var dataContext = await CreateDataConnectionAsync();
            try
            {
                entity.Id = await dataContext.InsertWithInt32IdentityAsync(entity);
            }
            // Ignore when we try insert foreign entity via InsertWithInt32IdentityAsync method
            catch (global::LinqToDB.SqlQuery.SqlException ex) when (ex.Message.StartsWith("Identity field must be defined for"))
            {
                await dataContext.InsertAsync(entity);
            }

            return entity;
        }

        /// <summary>
        /// Gets the name of a foreign key
        /// </summary>
        /// <param name="foreignTable">Foreign key table</param>
        /// <param name="foreignColumn">Foreign key column name</param>
        /// <param name="primaryTable">Primary table</param>
        /// <param name="primaryColumn">Primary key column name</param>
        /// <returns>Name of a foreign key</returns>
        public virtual string CreateForeignKeyName(string foreignTable, string foreignColumn, string primaryTable, string primaryColumn)
        {
            return $"FK_{foreignTable}_{foreignColumn}_{primaryTable}_{primaryColumn}";
        }

        /// <summary>
        /// Gets the name of an index
        /// </summary>
        /// <param name="targetTable">Target table name</param>
        /// <param name="targetColumn">Target column name</param>
        /// <returns>Name of an index</returns>
        public virtual string GetIndexName(string targetTable, string targetColumn)
        {
            return $"IX_{targetTable}_{targetColumn}";
        }

        #endregion

        #region Properties

        protected override IDataProvider LinqToDbDataProvider => _dataProvider.Value;

        #endregion
    }
}
