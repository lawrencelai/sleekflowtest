﻿
using FluentMigrator;
using FluentMigrator.Runner;
using FluentMigrator.Runner.Conventions;
using FluentMigrator.Runner.Initialization;
using FluentMigrator.Runner.Processors;
using LinqToDB;
using LinqToDB.Expressions;
using LinqToDB.Mapping;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Options;
using Newtonsoft.Json;
using OverCode.Core.Infrastructure;
using OverCode.Core.Infrastructure.DependencyManagement;
using OverCode.Data.Attributes;
using OverCode.Data.Mapping;
using OverCode.Data.Mapping.Builders;
using OverCode.Data.Migrations;
using OverCode.LinqToDb.Core;
using System.Linq;
using System.Linq.Expressions;

namespace OverCode.Data.Infrastructure
{
    public class DependencyRegistrar : IDependencyRegistrar
    {
        /// <summary>
        /// Register services and interfaces
        /// </summary>
        /// <param name="services">Collection of service descriptors</param>
        /// <param name="typeFinder">Type finder</param>
        /// <param name="configuration">Configuration</param>
        public virtual void Register(IServiceCollection services, ITypeFinder typeFinder, IConfiguration configuration)
        {
            services.Configure<DataSettings>(configuration.GetSection("OverCode.Data"));

            services.AddSingleton(serviceProvider =>
            {
                //we cannot use serviceProvider cause it will resolve IMigrationManager as scoped.
                var scope = serviceProvider.CreateScope();
                var migrationManager = scope.ServiceProvider.GetRequiredService<IMigrationManager>();
                var mappingSchema = new MappingSchema() { MetadataReader = new FluentMigratorMetadataReader(migrationManager) };

                var typeFinder = scope.ServiceProvider.GetService<ITypeFinder>();

                var deserializeMethod = MemberHelper.MethodOf(() => JsonConvert.DeserializeObject(null!, typeof(int)));

                foreach (var propertyInfo in typeFinder.GetAssemblies()
                    .SelectMany(a => a.GetTypes())
                    .SelectMany(t => t.GetProperties().Where(p => p.GetCustomAttributes(typeof(JsonContentAttribute), true).Any())))
                {
                    var inParam = Expression.Parameter(typeof(string));
                    var lambda = Expression.Lambda(
                        Expression.Convert(
                            Expression.Call(null, deserializeMethod, inParam,
                                Expression.Constant(propertyInfo.PropertyType)),
                            propertyInfo.PropertyType),
                        inParam);

                    mappingSchema.SetConvertExpression(typeof(string), propertyInfo.PropertyType, lambda);
                }

                return mappingSchema;
            });

            var mAssemblies = new AppDomainTypeFinder().FindClassesOfType<MigrationBase>()
                .Select(t => t.Assembly)
                .Where(assembly => !assembly.FullName.Contains("FluentMigrator.Runner"))
                .Distinct()
                .ToArray();

            services
                // add common FluentMigrator services
                .AddFluentMigratorCore()
                .AddScoped<IProcessorAccessor, OverCodeProcessorAccessor>()
                // set accessor for the connection string
                .AddScoped<IConnectionStringAccessor>(x => x.GetRequiredService<IOptions<DataSettings>>().Value)
                .AddScoped<IMigrationManager, MigrationManager>()
                .AddSingleton<IConventionSet, OverCodeConventionSet>()
                .ConfigureRunner(rb =>
                    rb.WithVersionTable(new MigrationVersionInfo()).AddSqlServer().AddMySql5().AddPostgres().AddSQLite()
                        // define the assembly containing the migrations
                        .ScanIn(mAssemblies).For.Migrations());

            services.AddTransient<IDataProviderManager, DataProviderManager>();
            services.AddTransient(serviceProvider =>
                serviceProvider.GetRequiredService<IDataProviderManager>().DataProvider);

            services.AddScoped(typeof(IRepository<>), typeof(EntityRepository<>));

            var entityBuilders = typeFinder.GetAssemblies()
                .SelectMany(a => a.GetTypes())
                .Where(t => t.IsClass && !t.IsAbstract
                && (t.BaseType != null && t.BaseType.IsGenericType && t.BaseType.GetGenericTypeDefinition() == typeof(OverCodeEntityBuilder<>)))
                .ToList();
            foreach (var entityBuilder in entityBuilders)
                services.AddScoped(entityBuilder, entityBuilder);
        }

        /// <summary>
        /// Order of this dependency registrar implementation
        /// </summary>
        public int Order => 5;
    }
}
