﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace OverCode.Data.Attributes
{
	[AttributeUsage(AttributeTargets.Property)]
	public class JsonContentAttribute : Attribute
	{
	}
}