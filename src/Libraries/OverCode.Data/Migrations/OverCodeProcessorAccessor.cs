﻿using System;
using System.Collections.Generic;
using System.Linq;
using FluentMigrator;
using FluentMigrator.Exceptions;
using FluentMigrator.Runner.Processors;
using Microsoft.Extensions.Options;

namespace OverCode.Data.Migrations
{
    /// <summary>
    /// An <see cref="IProcessorAccessor"/> implementation that selects one generator by data settings
    /// </summary>
    public class OverCodeProcessorAccessor : IProcessorAccessor
    {
        #region Fields

        #endregion

        #region Ctor

        public OverCodeProcessorAccessor(IEnumerable<IMigrationProcessor> processors,
            IOptions<DataSettings> dataSettings)
        {
            ConfigureProcessor(dataSettings.Value, processors.ToList());
        }

        #endregion

        #region Utils

        /// <summary>
        /// Configure processor
        /// </summary>
        /// <param name="processors">Collection of migration processors</param>
        protected virtual void ConfigureProcessor(DataSettings dataSettings, IList<IMigrationProcessor> processors)
        {
            if (processors.Count == 0)
                throw new ProcessorFactoryNotFoundException("No migration processor registered.");

            if (dataSettings is null)
                Processor = processors.FirstOrDefault();
            else
                Processor = dataSettings.DataProvider switch
                {
                    DataProviderType.SqlServer => FindGenerator(processors, "SqlServer"),
                    DataProviderType.MySql => FindGenerator(processors, "MySQL"),
                    DataProviderType.PostgreSQL => FindGenerator(processors, "Postgres"),
                    DataProviderType.SQLite => FindGenerator(processors, "SQLite"),
                    _ => throw new ProcessorFactoryNotFoundException(
                        $@"A migration generator for Data provider type {dataSettings.DataProvider} couldn't be found.")
                };
        }

        /// <summary>
        /// Gets single processor by DatabaseType or DatabaseTypeAlias
        /// </summary>
        /// <param name="processors">Collection of migration processors</param>
        /// <param name="processorsId">DatabaseType or DatabaseTypeAlias</param>
        /// <returns></returns>
        protected IMigrationProcessor FindGenerator(IList<IMigrationProcessor> processors,
            string processorsId)
        {
            if (processors.FirstOrDefault(p =>
                    p.DatabaseType.Equals(processorsId, StringComparison.OrdinalIgnoreCase) ||
                    p.DatabaseTypeAliases.Any(a => a.Equals(processorsId, StringComparison.OrdinalIgnoreCase))) is
                IMigrationProcessor processor)
                return processor;

            var generatorNames = string.Join(", ",
                processors.Select(p => p.DatabaseType).Union(processors.SelectMany(p => p.DatabaseTypeAliases)));

            throw new ProcessorFactoryNotFoundException(
                $@"A migration generator with the ID {processorsId} couldn't be found. Available generators are: {generatorNames}");
        }

        #endregion

        #region  Properties

        public IMigrationProcessor Processor { get; protected set; }

        #endregion
    }
}