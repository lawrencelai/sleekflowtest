﻿using FluentMigrator;
using FluentMigrator.Builders.Create;
using FluentMigrator.Builders.Create.Table;
using FluentMigrator.Builders.IfDatabase;
using FluentMigrator.Expressions;
using FluentMigrator.Infrastructure;
using FluentMigrator.Model;
using FluentMigrator.Runner;
using FluentMigrator.Runner.Initialization;
using OverCode.Core;
using OverCode.Core.Domain.Common;
using OverCode.Core.Infrastructure;
using OverCode.Data.Mapping.Builders;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Reflection;

namespace OverCode.Data.Migrations
{
    /// <summary>
    /// Represents the migration manager
    /// </summary>
    public partial class MigrationManager : IMigrationManager
    {
        #region Fields

        private readonly Dictionary<Type, Action<ICreateTableColumnAsTypeSyntax>> _typeMapping;
        private readonly IMigrationContext _migrationContext;
        private readonly ITypeFinder _typeFinder;
        private readonly IServiceProvider _serviceProvider;
        private readonly IMigrationRunnerConventions _migrationRunnerConventions;
        private readonly IFilteringMigrationSource _filteringMigrationSource;

        #endregion

        #region Ctor

        public MigrationManager(
            IMigrationContext migrationContext,
            ITypeFinder typeFinder,
            IServiceProvider serviceProvider,
            IMigrationRunnerConventions migrationRunnerConventions,
            IFilteringMigrationSource filteringMigrationSource)
        {
            _typeMapping = new Dictionary<Type, Action<ICreateTableColumnAsTypeSyntax>>
            {
                [typeof(int)] = c => c.AsInt32(),
                [typeof(long)] = c => c.AsInt64(),
                [typeof(string)] = c => c.AsString(int.MaxValue).Nullable(),
                [typeof(bool)] = c => c.AsBoolean(),
                [typeof(decimal)] = c => c.AsDecimal(18, 4),
                [typeof(DateTime)] = c => c.AsDateTime2(),
                [typeof(byte[])] = c => c.AsBinary(int.MaxValue),
                [typeof(Guid)] = c => c.AsGuid(),
                [typeof(double)] = c => c.AsDouble()
            };

            _migrationContext = migrationContext;
            _typeFinder = typeFinder;
            _serviceProvider = serviceProvider;
            _migrationRunnerConventions = migrationRunnerConventions;
            _filteringMigrationSource = filteringMigrationSource;
        }

        #endregion

        #region Utils

        /// <summary>
        /// Returns the instances for found types implementing FluentMigrator.IMigration
        /// </summary>
        /// <param name="assembly"></param>
        /// <returns>The instances for found types implementing FluentMigrator.IMigration</returns>
        private IEnumerable<IMigrationInfo> GetMigrations(Assembly assembly)
        {
            var migrations = _filteringMigrationSource.GetMigrations(t => assembly == null || t.Assembly == assembly) ?? Enumerable.Empty<IMigration>();

            return migrations.Select(m => _migrationRunnerConventions.GetMigrationInfoForMigration(m)).OrderBy(migration => migration.Version);
        }

        /// <summary>
        /// Defines the column specifications by default
        /// </summary>
        /// <param name="type">Type of entity</param>
        /// <param name="create">An expression builder for a FluentMigrator.Expressions.CreateTableExpression</param>
        /// <param name="propertyInfo">Property info</param>
        /// <param name="canBeNullable">The value indicating whether this column is nullable</param>
        protected virtual void DefineByOwnType(Type type, CreateTableExpressionBuilder create, PropertyInfo propertyInfo, bool canBeNullable = false)
        {
            var propType = propertyInfo.PropertyType;

            if (Nullable.GetUnderlyingType(propType) is Type uType)
            {
                propType = uType;
                canBeNullable = true;
            }

            if (!_typeMapping.ContainsKey(propType))
                return;

            if (type == typeof(string) || propType.FindInterfaces((t, o) => t.FullName?.Equals(o.ToString(), StringComparison.InvariantCultureIgnoreCase) ?? false, "System.Collections.IEnumerable").Any())
                canBeNullable = true;

            var column = create.WithColumn(propertyInfo.Name);
            _typeMapping[propType](column);

            if (canBeNullable)
                create.Nullable();
        }

        /// <summary>
        /// Retrieves expressions for building an entity table
        /// </summary>
        /// <param name="type">Type of entity</param>
        /// <param name="builder">An expression builder for a FluentMigrator.Expressions.CreateTableExpression</param>
        protected void RetrieveTableExpressions(Type type, CreateTableExpressionBuilder builder)
        {
            var tp = _typeFinder
                .FindClassesOfType(typeof(IEntityBuilder))
                .FirstOrDefault(t => t.BaseType?.GetGenericArguments().Contains(type) ?? false);

            if (tp != null)
                (_serviceProvider.GetService(tp) as IEntityBuilder)?.MapEntity(builder);

            var expression = builder.Expression;

            if (!expression.Columns.Any(c => c.IsPrimaryKey))
            {
                var pk = new ColumnDefinition
                {
                    Name = nameof(BaseEntity.Id),
                    Type = DbType.Int32,
                    IsIdentity = true,
                    TableName = type.Name,
                    ModificationType = ColumnModificationType.Create,
                    IsPrimaryKey = true
                };

                expression.Columns.Insert(0, pk);
                builder.CurrentColumn = pk;
            }

            var propertiesToAutoMap = type
                .GetProperties(BindingFlags.Instance | BindingFlags.DeclaredOnly | BindingFlags.Public | BindingFlags.SetProperty)
                .Where(p =>
                    !expression.Columns.Any(x => x.Name.Equals(p.Name, StringComparison.OrdinalIgnoreCase)));

            foreach (var prop in propertiesToAutoMap)
            {
                DefineByOwnType(type, builder, prop);
            }
        }

        /// <summary>
        /// Provides migration context with a null implementation of a processor that does not do any work
        /// </summary>
        /// <returns>The context of a migration while collecting up/down expressions</returns>
        protected IMigrationContext CreateNullMigrationContext()
        {
            return new MigrationContext(new NullIfDatabaseProcessor(), _migrationContext.ServiceProvider, null, null);
        }

        #endregion

        #region Methods

        /// <summary>
        /// Executes all found (and unapplied) migrations
        /// </summary>
        /// <param name="assembly">Assembly to find the migration</param>
        /// <param name="isUpdateProcess">Indicates whether the upgrade or installation process is ongoing. True - if an upgrade process</param>
        public void ApplyUpMigrations(Assembly assembly, bool isUpdateProcess = false)
        {
            if (assembly is null)
                throw new ArgumentNullException(nameof(assembly));

            var migrations = GetMigrations(assembly);

            bool needToExecute(IMigrationInfo migrationInfo1)
            {
                var skip = migrationInfo1.Migration.GetType().GetCustomAttributes(typeof(SkipMigrationAttribute)).Any() || isUpdateProcess && migrationInfo1.Migration.GetType()
                    .GetCustomAttributes(typeof(SkipMigrationOnUpdateAttribute)).Any();

                return !skip;
            }

            foreach (var migrationInfo in migrations.Where(needToExecute))
                (_serviceProvider.GetService(typeof(IMigrationRunner)) as IMigrationRunner).MigrateUp(migrationInfo.Version);
        }

        /// <summary>
        /// Retrieves expressions into ICreateExpressionRoot
        /// </summary>
        /// <param name="expressionRoot">The root expression for a CREATE operation</param>
        /// <typeparam name="TEntity">Entity type</typeparam>
        public virtual void BuildTable<TEntity>(ICreateExpressionRoot expressionRoot)
        {
            var type = typeof(TEntity);

            var builder = expressionRoot.Table(type.Name) as CreateTableExpressionBuilder;

            RetrieveTableExpressions(type, builder);
        }

        /// <summary>
        /// Gets create table expression for entity type
        /// </summary>
        /// <param name="type">Entity type</param>
        /// <returns>Expression to create a table</returns>
        public virtual CreateTableExpression GetCreateTableExpression(Type type)
        {
            var expression = new CreateTableExpression { TableName = type.Name };
            var builder = new CreateTableExpressionBuilder(expression, CreateNullMigrationContext());

            RetrieveTableExpressions(type, builder);

            return builder.Expression;
        }

        #endregion
    }
}