﻿

using LinqToDB.Mapping;
using Microsoft.Extensions.Options;
using OverCode.Core;
using OverCode.Data.DataProviders;
using System;

namespace OverCode.Data
{
    /// <summary>
    /// Represents the data provider manager
    /// </summary>
    public partial class DataProviderManager : IDataProviderManager
    {
        #region Fields

        private readonly DataSettings _dataSettings;
        private readonly MappingSchema _mappingSchema;

        #endregion

        #region Ctor

        public DataProviderManager(IOptions<DataSettings> dataSettings, MappingSchema mappingSchema)
        {
            _dataSettings = dataSettings.Value;
            _mappingSchema = mappingSchema;
        }

        #endregion

        #region Methods

        /// <summary>
        /// Gets data provider by specific type
        /// </summary>
        /// <param name="dataProviderType">Data provider type</param>
        /// <returns></returns>
        public IOverCodeDataProvider GetDataProvider()
        {
            var commandTimeout = _dataSettings.SQLCommandTimeout.HasValue ? _dataSettings.SQLCommandTimeout.Value : -1;

            switch (_dataSettings.DataProvider)
            {
                case DataProviderType.SqlServer:
                    return new MsSqlDataProvider(_dataSettings.ConnectionString, commandTimeout, _mappingSchema);
                case DataProviderType.MySql:
                    return new OverCodeMySqlDataProvider(_dataSettings.ConnectionString, commandTimeout, _mappingSchema);
                case DataProviderType.SQLite:
                    return new SQLiteDataProvider(_dataSettings.ConnectionString, commandTimeout, _mappingSchema);
                case DataProviderType.PostgreSQL:
                    return new PostgreSqlDataProvider(_dataSettings.ConnectionString, commandTimeout, _mappingSchema);
                default:
                    throw new Exception($"Not supported data provider name: '{_dataSettings.DataProvider}'");
            }
        }

        #endregion

        #region Properties

        /// <summary>
        /// Gets data provider
        /// </summary>
        public IOverCodeDataProvider DataProvider => GetDataProvider();

        #endregion
    }
}
