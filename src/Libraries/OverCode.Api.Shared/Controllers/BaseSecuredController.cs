﻿
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

namespace OverCode.Api.Shared.Controllers
{
    [Authorize]
    public abstract class BaseSecuredController : BaseController
    {
    }
}
