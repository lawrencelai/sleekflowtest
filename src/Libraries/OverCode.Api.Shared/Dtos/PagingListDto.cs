using System.Collections.Generic;

namespace OverCode.Common.Api.Dtos
{
    public class PagingListDto<T>
    {
        public int TotalCount { get; set; }

        public int TotalPages { get; set; }

        public IList<T> Items { get; set; }
    }
}