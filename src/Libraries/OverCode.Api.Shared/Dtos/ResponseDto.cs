
using Newtonsoft.Json;

namespace OverCode.Common.Api.Dtos
{
    public class ResponseDto
    {
        [JsonProperty("success")]
        public bool Success { get; set; }

        [JsonProperty("errorMessage")]
        public string ErrorMessage { get; set; }
    }

    public class ResponseDto<T>
    {
        [JsonProperty("success")]
        public bool Success { get; set; }

        [JsonProperty("errorMessage")]
        public string ErrorMessage { get; set; }

        [JsonProperty("entity")]
        public T Entity { get; set; }
    }

}
