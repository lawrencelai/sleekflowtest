﻿using Authentication.Services.Interface;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Options;
using Microsoft.IdentityModel.Tokens;
using OverCode.Api.Shared.Controllers;
using SleekFlow.ApiGateways.Web.Admin.HttpAggregator.Dtos.Authentication;
using SleekFlow.Users.Mappers;
using System;
using System.IdentityModel.Tokens.Jwt;
using System.Linq;
using System.Security.Claims;
using System.Text;
using System.Threading.Tasks;
using UserRoles.Services.Interface;

namespace SleekFlow.ApiGateways.Web.Admin.HttpAggregator.Controllers
{
    public class AuthenticateController : BaseController
    {
        private readonly JwtSettings _jwtSettings;
        private readonly IUserAuthenticationService _userAuthenticationService;
        private readonly IUserRoleService _userRoleService;


        public AuthenticateController(IOptions<JwtSettings> jwtSettings,
            IUserAuthenticationService userAuthenticationService,
            IUserRoleService userRoleService
           )
        {
            _jwtSettings = jwtSettings.Value;
            _userAuthenticationService = userAuthenticationService;
            _userRoleService = userRoleService;
        }

        [HttpPost("Login")]
        public async Task<IActionResult> Login(LoginDto model)
        {
            var user = await _userAuthenticationService.AuthenticateUserAsync(model.Username, model.Password);

            var authenticatedModel = new AuthenticatedDto()
            {
                User = UserMapper.MapToSimpleUserDto(user),
                PermissionNames = await _userRoleService.GetPermissionNamesByUserIdAsync(user.Id)
            };

       

            var expiresOnUtc = DateTime.UtcNow.AddMinutes(_jwtSettings.TokenValidityInMinutes);
            var tokenHandler = new JwtSecurityTokenHandler();
            var tokenKey = Encoding.UTF8.GetBytes(_jwtSettings.SecretKey);
            var tokenDescriptor = new SecurityTokenDescriptor
            {
                Subject = new ClaimsIdentity(new Claim[]
                {
                    new Claim(ClaimTypes.NameIdentifier, user.Username)
                }),
                Expires = expiresOnUtc,
                Issuer = _jwtSettings.Issuer,
                SigningCredentials = new SigningCredentials(new SymmetricSecurityKey(tokenKey), SecurityAlgorithms.HmacSha256Signature)
            };
            var token = tokenHandler.CreateToken(tokenDescriptor);

            authenticatedModel.AccessToken = tokenHandler.WriteToken(token);
            authenticatedModel.ExpiresOnUtc = expiresOnUtc;
            authenticatedModel.Roles = _userRoleService.GetUserRolesByUserIdAsync(user.Id).Result.Select(x=>x.RoleName).ToList();

            return Ok(authenticatedModel);
        }
    }
}
