﻿using SleekFlow.ApiGateways.Web.Admin.HttpAggregator.Infrastructure;
using Job.Dtos;
using Job.Dtos.Enum;
using Job.Services.Interface;
using Microsoft.AspNetCore.Mvc;
using OverCode.Api.Shared.Controllers;
using OverCode.Common.Api.Dtos;
using System;
using System.Linq;
using System.Threading.Tasks;
using UserRoles.Services.Interface;
using Users.Services.Interface;
using Web.Admin.HttpAggregator.Dtos.Job;
using Web.Admin.HttpAggregator.Helpers;
using Microsoft.Extensions.Logging;
using OverCode.Core.Events;

namespace SleekFlow.ApiGateways.Web.Admin.HttpAggregator.Controllers
{
    [Route("Job")]
    public class JobController : BaseSecuredController
    {
        private readonly WorkContext _workContext;
        private readonly IUserService _userService;
        private readonly IJobService _jobService;
        private readonly IUserRoleService _userRoleService;
        private readonly ILogger<JobController> _logger;

        public JobController(
            WorkContext workContext,
            IUserService userService,
            IJobService jobService,
            IUserRoleService userRoleService,
            ILogger<JobController> logger
           )
        {
            _jobService = jobService;
            _workContext = workContext;
            _userService = userService;
            _userRoleService = userRoleService;
            _logger = logger;
        }

        [HttpPost("List")]
        public virtual async Task<IActionResult> List(SearchJobDto model)
        {
            var result = new ResponseDto<PagingListDto<JobDto>>()
            {
                Success = true,
            };

            try
            {
                if (!await _userRoleService.HasPermissionAsync(_workContext.User.Id, PermissionDefaults.ViewJob))
                    return Unauthorized();

                var job = await _jobService.SearchJobAsync(
                    createdDateFrom: model.StartDate == null ? null : Convert.ToDateTime(model.StartDate),
                    createdDateTo: model.EndDate == null ? null : Convert.ToDateTime(model.EndDate),
                    status: model.Status,
                    createdBy: model.CreatedBy,
                    sortBy: model.SortBy
                );

                var jobs = new PagingListDto<JobDto>()
                {
                    Items = job.Select(x =>
                    {
                        var user = _userService.GetUserByIdAsync(x.CreatedBy).Result;

                        var result = new JobDto()
                        {
                            Id = x.Id,
                            Name = x.Name,
                            DueDate = x.DueDateUtc != null ? DateTimeHelper.ToLocalDateTime(x.DueDateUtc.Value).ToString("dd-MMM-yyy h:mmtt") : "",
                            CreatedDateTime = DateTimeHelper.ToLocalDateTime(x.CreatedDateTimeUtc).ToString("dd-MMM-yy h:mmtt"),
                            CompletedDateTime = x.CompletedDateTimeUtc != null ? DateTimeHelper.ToLocalDateTime(x.CompletedDateTimeUtc.Value).ToString("dd-MMM-yyy h:mmtt") : "",
                            Status = x.Status.ToString(),
                            CreatedBy = user != null ? user.Name : ""
                        };

                        return result;
                    }).ToList(),
                    TotalCount = job.TotalCount,
                    TotalPages = job.TotalPages
                };

                result.Entity = jobs;
            }
            catch (Exception ex)
            {
                _logger.LogError(ex.Message, ex);
                result.Success = false;
            }

            return Ok(result);
        }

        [HttpGet("{id}")]
        public virtual async Task<IActionResult> View(int id)
        {
            var result = new ResponseDto<JobDto>()
            {
                Success = true,
            };

            try
            {
                if (!await _userRoleService.HasPermissionAsync(_workContext.User.Id, PermissionDefaults.ViewJob))
                    return Unauthorized();

                var job = await _jobService.GetJobByIdAsync(id);
                var jobDto = new JobDto();

                if (job != null)
                {
                    var user = _userService.GetUserByIdAsync(job.CreatedBy).Result;

                    jobDto = new JobDto()
                    {
                        Id = job.Id,
                        Name = job.Name,
                        Description = job.Description,
                        DueDate = job.DueDateUtc != null ? DateTimeHelper.ToLocalDateTime(job.DueDateUtc.Value).ToString("dd-MMM-yyy h:mmtt") : "",
                        CreatedDateTime = DateTimeHelper.ToLocalDateTime(job.CreatedDateTimeUtc).ToString("dd-MMM-yy h:mmtt"),
                        CompletedDateTime = job.CompletedDateTimeUtc != null ? DateTimeHelper.ToLocalDateTime(job.CompletedDateTimeUtc.Value).ToString("dd-MMM-yyy h:mmtt") : "",
                        Status = ((JobStatus)job.Status).ToString(),
                        StatusId = (int)job.Status,
                        CreatedById = job.CreatedBy,
                        CreatedBy = user != null ? user.Name : ""
                    };

                    result.Entity = jobDto;
                }
                else
                {
                    return BadRequest("Job not found");
                }
            }
            catch (Exception ex)
            {
                _logger.LogError(ex.Message, ex);
                result.Success = false;
            }

            return Ok(result);
        }

        [HttpPost("Create")]
        public virtual async Task<IActionResult> Create(CreateJobDto model)
        {
            var result = new ResponseDto()
            {
                Success = true,
            };

            try
            {

                if (!await _userRoleService.HasPermissionAsync(_workContext.User.Id, PermissionDefaults.AddJob))
                    return Unauthorized();

                await _jobService.CreateJob(model);
            }
            catch (Exception ex)
            {
                _logger.LogError(ex.Message, ex);
                result.Success = false;
            }

            return Ok(result);
        }

        [HttpPost("Edit")]
        public virtual async Task<IActionResult> Edit(EditJobDto model)
        {
            var result = new ResponseDto()
            {
                Success = true,
            };

            try
            {

                if (!await _userRoleService.HasPermissionAsync(_workContext.User.Id, PermissionDefaults.EditJob))
                    return Unauthorized();

                var job = await _jobService.GetJobByIdAsync(model.Id);

                if (job == null)
                    return BadRequest("Job not found");


                job.Name = model.Name;
                job.Description = model.Description;
                job.DueDateUtc = model.DueDateUtc;
                job.Status = model.Status;

                if (job.Status == (int)JobStatus.Completed)
                    job.CompletedDateTimeUtc = DateTime.UtcNow;


                await _jobService.UpdateJobAsync(job);
            }
            catch (Exception ex)
            {
                _logger.LogError(ex.Message, ex);
                result.Success = false;
            }

            return Ok(result);
        }

    }
}
