﻿using Microsoft.AspNetCore.Http;
using SleekFlow.Users.Dtos;
using SleekFlow.Users.Services;
using System.Linq;
using System.Security.Claims;
using SleekFlow.UserRoles.Services;
using Users.Services.Interface;
using UserRoles.Services.Interface;

namespace SleekFlow.ApiGateways.Web.Admin.HttpAggregator.Infrastructure
{
    public partial class WorkContext
    {
        #region Fields

        private readonly IHttpContextAccessor _httpContextAccessor;
        private readonly IUserService _userService;
        private readonly IUserRoleService _userRoleService;


        private string _cachedUsername;
        private User _cacheduser;
        private string _cachedRole;

        #endregion

        #region Ctor

        public WorkContext(
            IHttpContextAccessor httpContextAccessor,
            IUserService userService,
            IUserRoleService userRoleService
            )
        {
            _httpContextAccessor = httpContextAccessor;
            _userService = userService;
            _userRoleService = userRoleService;
        }

        #endregion

        #region Utilities

        #endregion

        #region Properties

        public virtual string Username
        {
            get
            {
                if (_cachedUsername != null)
                    return _cachedUsername;

                var claims = _httpContextAccessor?.HttpContext?.User?.Claims;
                if (claims == null)
                    return null;

                _cachedUsername = claims.FirstOrDefault(c => c.Type == ClaimTypes.NameIdentifier)?.Value;

                return _cachedUsername;
            }
        }

        public virtual User User
        {
            get
            {
                if (_cacheduser != null)
                    return _cacheduser;

                if (string.IsNullOrEmpty(Username))
                    return null;

                _cacheduser = _userService.GetUserByUsernameAsync(Username).Result;

                return _cacheduser;
            }
        }

        public virtual string Role
        {
            get
            {
                if (_cachedRole != null)
                    return _cachedRole;


                var user = _userService.GetUserByUsernameAsync(Username).Result;
                _cachedRole = _userRoleService.GetUserRolesByUserIdAsync(user.Id).Result.FirstOrDefault().RoleName;

                return _cachedRole;
            }
        }
        #endregion
    }
}
