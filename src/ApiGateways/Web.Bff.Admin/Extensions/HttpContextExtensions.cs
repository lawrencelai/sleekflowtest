﻿using Microsoft.AspNetCore.Http;
using System;
using System.IO;
using System.Text;
using System.Threading.Tasks;

namespace SleekFlow.ApiGateways.Web.Admin.HttpAggregator.Extensions
{
    public static class HttpContextExtensions
    {
        #region Methods

        public static async Task<string> ReadBodyAsync(this HttpRequest request)
        {
            // IMPORTANT: Ensure the requestBody can be read multiple times.
            request.EnableBuffering();

            //// IMPORTANT: Leave the body open so the next middleware can read it.
            //using (var reader = new StreamReader(context.Request.Body, encoding: Encoding.UTF8,
            //    detectEncodingFromByteOrderMarks: false, leaveOpen: true))

            var buffer = new byte[Convert.ToInt32(request.ContentLength)];
            await request.Body.ReadAsync(buffer, 0, buffer.Length);
            var bodyAsText = Encoding.UTF8.GetString(buffer);

            // IMPORTANT: Reset the request body stream position so the next middleware can read it
            request.Body.Seek(0, SeekOrigin.Begin);

            return bodyAsText;
        }

        public static async Task<string> ReadBodyAsync(this HttpResponse response)
        {
            response.Body.Seek(0, SeekOrigin.Begin);
            var bodyAsText = await new StreamReader(response.Body).ReadToEndAsync();
            response.Body.Seek(0, SeekOrigin.Begin);

            return bodyAsText;
        }

        public static async Task<string> ReadResponseBodyAsync(this HttpContext httpContext, MemoryStream responseBodyStream)
        {
            responseBodyStream.Position = 0;
            var bodyAsText = await new StreamReader(responseBodyStream).ReadToEndAsync();

            //write again to response stream so other middleware can read it
            responseBodyStream.Position = 0;

            return bodyAsText;
        }

        #endregion
    }
}