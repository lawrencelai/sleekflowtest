﻿using FluentValidation;
using Job.Dtos;

namespace Web.Admin.HttpAggregator.Validators.Job
{
    public class CreateJobDtoValidator : AbstractValidator<CreateJobDto>
    {
        public CreateJobDtoValidator()
        {

            RuleFor(x => x.Name).NotEmpty().Length(5,30).WithMessage(x => $"Name should contain 10-30 character");
            RuleFor(x => x.Description).NotEmpty().Length(5, 1000).WithMessage(x => $"Description should contain 50-1000 character"); ;
            RuleFor(x => x.CreatedBy).NotEmpty();
        }
    }
}
