﻿using FluentValidation;
using Job.Dtos;

namespace Web.Admin.HttpAggregator.Validators.Job
{
    public class EditJobDtoValidator : AbstractValidator<EditJobDto>
    {
        public EditJobDtoValidator()
        {

            RuleFor(x => x.Id).NotEmpty();
            RuleFor(x => x.Name).NotEmpty().Length(5, 30).WithMessage(x => $"Name should contain 10-30 character");
            RuleFor(x => x.Description).NotEmpty().Length(5, 1000).WithMessage(x => $"Description should contain 50-1000 character"); ;
        }
    }
}
