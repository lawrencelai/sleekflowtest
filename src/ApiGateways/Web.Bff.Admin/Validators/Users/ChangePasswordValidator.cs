﻿using FluentValidation;
using SleekFlow.ApiGateways.Web.Admin.HttpAggregator.Dtos.Users;

namespace SleekFlow.ApiGateways.Web.Admin.HttpAggregator.Validators.Users
{
    public class ChangePasswordValidator : AbstractValidator<ChangePasswordDto>
    {
        public ChangePasswordValidator()
        {
            RuleFor(x => x.OldPassword).NotEmpty();
            RuleFor(x => x.NewPassword).NotEmpty();
        }
    }
}