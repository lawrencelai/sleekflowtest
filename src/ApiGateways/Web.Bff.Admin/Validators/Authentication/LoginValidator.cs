﻿using FluentValidation;
using SleekFlow.ApiGateways.Web.Admin.HttpAggregator.Dtos.Authentication;

namespace SleekFlow.ApiGateways.Web.Admin.HttpAggregator.Validators.Authentication
{
    public class LoginValidator : AbstractValidator<LoginDto>
    {
        public LoginValidator()
        {
            RuleFor(x => x.Username).NotEmpty();
            RuleFor(x => x.Password).NotEmpty();
        }
    }
}