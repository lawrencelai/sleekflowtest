
namespace SleekFlow.ApiGateways.Web.Admin.HttpAggregator
{
    public static class PermissionDefaults
    {

        public const string ViewJob = "ViewJob";

        public const string AddJob = "AddJob";

        public const string EditJob = "EditJob";

    }
}
