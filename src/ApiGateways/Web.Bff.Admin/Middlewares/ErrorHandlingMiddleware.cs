﻿using SleekFlow.ApiGateways.Web.Admin.HttpAggregator.Extensions;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using Newtonsoft.Json;
using OverCode.Common.Api.Dtos;
using OverCode.Core;
using System;
using System.IO;
using System.Net;
using System.Threading.Tasks;

//using SleekFlow.ApiGateways.Web.Admin.HttpAggregator.Models;

namespace SleekFlow.ApiGateways.Web.Admin.HttpAggregator.Middlewares
{
    public class ErrorHandlingMiddleware
    {
        #region Fields

        private readonly RequestDelegate _next;
        private readonly IServiceScopeFactory _serviceScopeFactory;

        #endregion

        #region Ctor

        public ErrorHandlingMiddleware(
            RequestDelegate next,
            IServiceScopeFactory serviceScopeFactory) {
            _next = next;
            _serviceScopeFactory = serviceScopeFactory;
        }

        #endregion

        #region Methods

        /// <summary>
        /// Invoke middleware actions
        /// </summary>
        /// <param name="context">HTTP context</param>
        /// <returns>Task</returns>
        public async Task Invoke(HttpContext context, ILogger<ErrorHandlingMiddleware> logger)
        {
            var request = $"{context.Request.Scheme} {context.Request.Host}{context.Request.Path} {context.Request.QueryString} {await context.Request.ReadBodyAsync()}";
            try
            {
                //using (var scope = _serviceScopeFactory.CreateScope()) {
                //    var loggingService = scope.ServiceProvider.GetRequiredService<LoggingService>();
                //    loggingService.CreateLog(request);
                //}

                await _next(context);
            }
            catch (Exception ex)
            {
                await HandleException(context, ex, logger, request);
            }
        }

        private async Task HandleException(HttpContext context, Exception ex, ILogger<ErrorHandlingMiddleware> logger,
            string requestDetails)
        {
            var code = HttpStatusCode.OK;
            var errorMessage = ex.Message;

            // Specify different custom exceptions here
            if (!(ex is OverCodeException))
            {
                logger.LogError(ex, "Unhandled Exception\nRequest : " + requestDetails);
                code = HttpStatusCode.InternalServerError;
                errorMessage = "An error has occurred. Please try again later.";
            }

            var result = JsonConvert.SerializeObject(new ResponseDto()
            {
                Success = false,
                ErrorMessage = errorMessage
            });

            context.Response.ContentType = "application/json";
            context.Response.StatusCode = (int)code;

            await context.Response.WriteAsync(result);
        }

        #endregion
    }
}