﻿
using SleekFlow.Users.Dtos;
using System;
using System.Collections.Generic;

namespace SleekFlow.ApiGateways.Web.Admin.HttpAggregator.Dtos.Authentication
{
    public class AuthenticatedDto
    {
        public AuthenticatedDto()
        {
            PermissionNames = new List<string>();
        }

        public SimpleUserDto User { get; set; }

        public IList<string> PermissionNames { get; set; }

        public string AccessToken { get; set; }

        public DateTime ExpiresOnUtc { get; set; }

        public IList<string> Roles { get; set; }

    }
}