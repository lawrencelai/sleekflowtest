﻿
namespace SleekFlow.ApiGateways.Web.Admin.HttpAggregator.Dtos.Authentication
{
    public class LoginDto
    {
        public string Username { get; set; }
        public string Password { get; set; }
    }
}