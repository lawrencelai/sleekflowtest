﻿using Job.Dtos.Enum;

namespace Web.Admin.HttpAggregator.Dtos.Job
{
    public class JobDto
    {
        public int Id { get; set; }

        public string Name { get; set; }

        public string Description { get; set; }

        public string DueDate { get; set; }

        public string Status { get; set; }

        public int StatusId { get; set; }

        public string CreatedBy { get; set; }

        public int CreatedById { get; set; }

        public string CreatedDateTime { get; set; }

        public string CompletedDateTime { get; set; }
    }
}
