﻿using System;

namespace SleekFlow.ApiGateways.Web.Admin.HttpAggregator.Dtos.Users
{
    public class ChangePasswordDto
    {
        public string OldPassword { get; set; }

        public string NewPassword { get; set; }
    }
}