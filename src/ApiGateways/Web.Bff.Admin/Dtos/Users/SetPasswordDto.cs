﻿using System;

namespace SleekFlow.ApiGateways.Web.Admin.HttpAggregator.Dtos.Users
{
    public class SetPasswordDto
    {
        public string NewPassword { get; set; }
    }
}