﻿using DocumentFormat.OpenXml.Drawing.Charts;
using System;

namespace Web.Admin.HttpAggregator.Dtos.Users
{
    public class UserDto
    {
        public int Id { get; set; }

        public string Code { get; set; }

        public string Username { get; set; }

        public string Email { get; set; }

        public string HandphoneNumber { get; set; }

        public string Name { get; set; }

        public bool Active { get; set; }

        public string Role { get; set; }

        public string LastLoginOnUtc { get; set; }

        public bool IsCommissionSchemeAllowed { get; set; }

        public decimal MotorCommissionAmount { get; set; }

        public int MotorCommissionScheme { get; set; }

        public int MotorCommissionType { get; set; }

        public bool MotorExcludedPaymentGatewayCharges { get; set; }
    }
}
