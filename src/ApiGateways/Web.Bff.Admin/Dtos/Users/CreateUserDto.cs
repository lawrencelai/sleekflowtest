﻿using System;

namespace SleekFlow.ApiGateways.Web.Admin.HttpAggregator.Dtos.Users
{
    public class CreateUserDto
    {
        public string Username { get; set; }

        public string Email { get; set; }

        public string HandphoneNumber { get; set; }
        
        public string Name { get; set; }

        public bool Active { get; set; }

        public string Password { get; set; }

        public string Code { get; set; }

        public string Role { get; set; }

        public bool IsCommissionSchemeAllowed { get; set; }

        public decimal MotorCommissionAmount { get; set; }

        public int MotorCommissionScheme { get; set; }

        public int MotorCommissionType { get; set; }

        public bool MotorExcludedPaymentGatewayCharges { get; set; }
    }
}