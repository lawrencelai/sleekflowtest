﻿using System.Runtime.InteropServices;
using System;

namespace Web.Admin.HttpAggregator.Helpers
{
    public static class DateTimeHelper
    {
        private const string SINGAPORE_STANDARD_TIME = "Singapore Standard Time";
        private const string LINUX_SINGAPORE_STANDARD_TIME = "Asia/Kuala_Lumpur";
        public static DateTime ToLocalDateTime(DateTime utc)
        {
            if (RuntimeInformation.IsOSPlatform(OSPlatform.Windows))
            {
                return TimeZoneInfo.ConvertTimeFromUtc(utc, TimeZoneInfo.FindSystemTimeZoneById(SINGAPORE_STANDARD_TIME));
            }
            else
            {
                return TimeZoneInfo.ConvertTimeFromUtc(utc, TimeZoneInfo.FindSystemTimeZoneById(LINUX_SINGAPORE_STANDARD_TIME));
            }
        }

        public static DateTime ToUtcDateTime(DateTime local)
        {
            try
            {
                return TimeZoneInfo.ConvertTimeToUtc(local, TimeZoneInfo.FindSystemTimeZoneById(SINGAPORE_STANDARD_TIME));
            }
            catch (Exception ex)
            {
                //if fails to convert, we use default to universal
                return local.ToUniversalTime();
            }
        }
    }
}
