﻿using SleekFlow.Users.Dtos;
using System;

namespace SleekFlow.Authentication.Dtos
{
    public class AuthenticatedDto
    {
        public SimpleUserDto User { get; set; }
        public string AccessToken { get; set; }
        public DateTime ExpiresOnUtc { get; set; }
    }
}