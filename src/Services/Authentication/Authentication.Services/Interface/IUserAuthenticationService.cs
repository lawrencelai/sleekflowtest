﻿using SleekFlow.Users.Dtos;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Authentication.Services.Interface
{
    public interface IUserAuthenticationService
    {
        Task<User>  AuthenticateUserAsync(string username, string password);
    }
}
