﻿using Authentication.Services.Interface;
using OverCode.Core;
using SleekFlow.Users.Dtos;
using System;
using System.Threading.Tasks;
using Users.Services.Interface;

namespace SleekFlow.Authentication.Services
{
    public class UserAuthenticationService : IUserAuthenticationService
    {
        #region Fields

        private readonly IUserPasswordService _userPasswordService;
        private readonly IUserService _userService;

        #endregion

        #region Ctor

        public UserAuthenticationService(
            IUserPasswordService userPasswordService,
            IUserService userService)
        {
            _userPasswordService = userPasswordService;
            _userService = userService;
        }

        #endregion

        public virtual async Task<User> AuthenticateUserAsync(string username, string password)
        {
            var user = await _userService.GetUserByUsernameAsync(username);

            if (user == null)
                throw new OverCodeException("Invalid username / password");

            if (!await _userPasswordService.PasswordMatchAsync(user.PasswordHash, user.PasswordSalt, password))
                throw new OverCodeException("Invalid username / password");

            user.LastLoginOnUtc = DateTime.Now;
            await _userService.UpdateUserAsync(user);

            return user;
        }

    }
}