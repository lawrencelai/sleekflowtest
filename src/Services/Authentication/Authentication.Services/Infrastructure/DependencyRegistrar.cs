﻿using Authentication.Services.Interface;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using OverCode.Core.Infrastructure;
using OverCode.Core.Infrastructure.DependencyManagement;

namespace SleekFlow.Authentication.Services.Infrastructure
{
    public class DependencyRegistrar : IDependencyRegistrar
    {
        /// <summary>
        /// Register services and interfaces
        /// </summary>
        /// <param name="services">Collection of service descriptors</param>
        /// <param name="typeFinder">Type finder</param>
        /// <param name="configuration">Configuration</param>
        public virtual void Register(IServiceCollection services, ITypeFinder typeFinder, IConfiguration configuration)
        {
            services.AddScoped<IUserAuthenticationService, UserAuthenticationService>(); 
        }

        /// <summary>
        /// Order of this dependency registrar implementation
        /// </summary>
        public int Order => 10;
    }
}