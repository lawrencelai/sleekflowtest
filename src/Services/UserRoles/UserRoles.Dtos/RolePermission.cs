using OverCode.Core;
using System;

namespace SleekFlow.UserRoles.Dtos
{
    public class RolePermission : BaseEntity
    {
        public string RoleName { get; set; }

        public string PermissionName { get; set; }
    }
}