using OverCode.Core;
using System;

namespace SleekFlow.UserRoles.Dtos
{
    public class UserRole : BaseEntity
    {
        public int UserId { get; set; }

        public string RoleName { get; set; }
    }
}