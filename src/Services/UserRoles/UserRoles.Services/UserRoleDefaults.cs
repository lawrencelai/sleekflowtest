﻿using OverCode.Core.Caching;

namespace SleekFlow.UserRoles.Services
{
    public class UserRoleDefaults
    {
        public static CacheKey UserPermissionsByUserIdCacheKey => new CacheKey("userpermission.by.userid.{0}");

    }
}
