﻿using OverCode.Core.Caching;
using OverCode.Core.Events;
using SleekFlow.UserRoles.Dtos;
using System.Threading.Tasks;

namespace SleekFlow.UserRoles.Services.Cache
{
    public partial class UserRoleCacheEventConsumer : CacheEventConsumer<UserRole>
    {
        public UserRoleCacheEventConsumer(IStaticCacheManager staticCacheManager) : base(staticCacheManager)
        {
        }

        protected override async Task ClearCacheAsync(UserRole entity)
        {
            await RemoveAsync(UserRoleDefaults.UserPermissionsByUserIdCacheKey, entity.UserId);
        }
    }
}