﻿using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using OverCode.Core.Infrastructure;
using OverCode.Core.Infrastructure.DependencyManagement;
using SleekFlow.UserRoles.Services;
using UserRoles.Services.Interface;

namespace SleekFlow.UserRoles.Services.Infrastructure
{
    public class DependencyRegistrar : IDependencyRegistrar
    {
        /// <summary>
        /// Register services and interfaces
        /// </summary>
        /// <param name="services">Collection of service descriptors</param>
        /// <param name="typeFinder">Type finder</param>
        /// <param name="configuration">Configuration</param>
        public virtual void Register(IServiceCollection services, ITypeFinder typeFinder, IConfiguration configuration)
        {
            services.AddScoped<IUserRoleService, UserRoleService>();
        }

        /// <summary>
        /// Order of this dependency registrar implementation
        /// </summary>
        public int Order => 10;
    }
}