﻿using SleekFlow.UserRoles.Dtos;
using OverCode.Core.Caching;
using OverCode.LinqToDb.Core;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Dynamic.Core;
using System.Threading.Tasks;
using UserRoles.Services.Interface;

namespace SleekFlow.UserRoles.Services
{
    public class UserRoleService: IUserRoleService
    {
        #region Fields

        private readonly IRepository<UserRole> _userRoleRepository;
        private readonly IRepository<RolePermission> _rolePermissionRepository;
        private readonly IStaticCacheManager _staticCacheManager;

        #endregion

        #region Ctor

        public UserRoleService(IRepository<UserRole> userRoleRepository,
            IRepository<RolePermission> rolePermissionRepository,
            IStaticCacheManager staticCacheManager)
        {
            _userRoleRepository = userRoleRepository;
            _rolePermissionRepository = rolePermissionRepository;
            _staticCacheManager = staticCacheManager;
        }

        #endregion

        public virtual async Task<IList<string>> GetPermissionNamesByUserIdAsync(int userId)
        {
            var key = _staticCacheManager.PrepareKeyForDefaultCache(UserRoleDefaults.UserPermissionsByUserIdCacheKey, userId);
            return await _staticCacheManager.GetAsync(key, async () =>
            {
                return await (from ur in _userRoleRepository.Table
                             join rp in _rolePermissionRepository.Table on ur.RoleName equals rp.RoleName
                             where ur.UserId == userId
                             select rp.PermissionName)
                             .ToListAsync();
            });
        }

        public virtual async Task<bool> HasPermissionAsync(int userId, string permissionName)
        {
            var userPermissions = await GetPermissionNamesByUserIdAsync(userId);

            return userPermissions.Any(x => x == permissionName);
        }

        public virtual async Task InsertUserRoleAsync(UserRole userRole)
        {
            await _userRoleRepository.InsertAsync(userRole);
        }


        public virtual async Task<IList<UserRole>> GetUserRolesByUserIdAsync(int userId)
        {
           return  await _userRoleRepository.Table.Where(x => x.UserId == userId).ToListAsync();
        }


        public virtual async Task UpdateUserRoleAsync(UserRole userRole)
        {
            if (userRole == null)
                throw new ArgumentNullException("user");

            await _userRoleRepository.UpdateAsync(userRole);
        }

    }
}