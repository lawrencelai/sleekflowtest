﻿using SleekFlow.UserRoles.Dtos;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace UserRoles.Services.Interface
{
    public interface IUserRoleService
    {
        Task<IList<string>> GetPermissionNamesByUserIdAsync(int userId);

        Task<bool>  HasPermissionAsync(int userId, string permissionName);

        Task InsertUserRoleAsync(UserRole userRole);

        Task<IList<UserRole>> GetUserRolesByUserIdAsync(int userId);

        Task UpdateUserRoleAsync(UserRole userRole);
    }
}
