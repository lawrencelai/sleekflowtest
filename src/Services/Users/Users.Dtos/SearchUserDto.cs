using OverCode.Core;
using System;

namespace SleekFlow.Users.Dtos
{
    public class SearchUserDto
    {
        public string Username { get; set; }

        public string Name { get; set; }

        public string SortBy { get; set; }

        public int PageIndex { get; set; }

        public int PageSize { get; set; }
    }
}