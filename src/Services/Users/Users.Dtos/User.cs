using OverCode.Core;
using System;

namespace SleekFlow.Users.Dtos
{
    public class User : BaseEntity
    {
        public string Username { get; set; }

        public string Code { get; set; }

        public string Name { get; set; }

        public string PasswordHash { get; set; }

        public string PasswordSalt { get; set; }

        public DateTime? LastLoginOnUtc { get; set; }

        public bool Active { get; set; }
    }
}