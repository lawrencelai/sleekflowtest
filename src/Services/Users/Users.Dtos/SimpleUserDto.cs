using OverCode.Core;
using System;

namespace SleekFlow.Users.Dtos
{
    public class SimpleUserDto 
    {
        public int Id { get; set; }

        public string Username { get; set; }

        public DateTime? LastLoginOnUtc { get; set; }

        public bool Active { get; set; }
    }
}