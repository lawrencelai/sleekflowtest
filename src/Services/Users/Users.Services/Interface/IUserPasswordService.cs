﻿using SleekFlow.Users.Dtos;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Users.Services.Interface
{
    public interface IUserPasswordService
    {
        Task ValidatePasswordAsync(string password);

        Task SetPasswordAsync(User user, string password);

        Task<bool> PasswordMatchAsync(string passwordHash, string passwordSalt, string enteredPassword);

        Task ChangePasswordWithCurrentPasswordAsync(User user, string currentPassword, string newPassword);
    }
}
