﻿using SleekFlow.Users.Dtos;
using OverCode.Core;
using System.Threading.Tasks;

namespace Users.Services.Interface
{
    public interface IUserService
    {
        Task<User> GetUserByIdAsync(int id);

        Task<User> GetUserByUsernameAsync(string username);

        Task<IPagedList<User>> SearchUsersAsync(string username = null, string name = null,
            string sortBy = null, int pageIndex = 0, int pageSize = int.MaxValue, bool getOnlyTotalCount = false);

        Task<User> CreateUserAsync(string username, string name, string code);

        Task UpdateUserAsync(User user);

        Task<User> UpdateUserUsernameAsync(int userId, string username);
    }
}
