﻿using OverCode.Core.Caching;
using SleekFlow.Users.Dtos;

namespace SleekFlow.Users.Mappers
{
    public static class UserMapper
    {
        public static SimpleUserDto MapToSimpleUserDto(User user)
        {
            return new SimpleUserDto()
            {
                Id= user.Id,
                Active = user.Active,
                LastLoginOnUtc = user.LastLoginOnUtc,
                Username = user.Username,
            };
        }

    }
}
