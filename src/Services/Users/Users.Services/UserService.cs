﻿using SleekFlow.Users.Dtos;
using OverCode.Core;
using OverCode.Core.Caching;
using OverCode.LinqToDb.Core;
using System;
using System.Linq;
using System.Linq.Dynamic.Core;
using System.Threading.Tasks;
using Users.Services.Interface;

namespace SleekFlow.Users.Services
{
    public class UserService : IUserService
    {
        #region Fields

        private readonly IRepository<User> _userRepository;
        private readonly IStaticCacheManager _staticCacheManager;

        #endregion

        #region Ctor

        public UserService(
            IRepository<User> userRepository,
            IStaticCacheManager staticCacheManager)
        {
            _userRepository = userRepository;
            _staticCacheManager = staticCacheManager;
        }

        #endregion

        public virtual async Task<User> GetUserByIdAsync(int id)
        {
            return await _userRepository.GetByIdAsync(id, cache => default);
        }

        public virtual async Task<User> GetUserByUsernameAsync(string username)
        {
            var key = _staticCacheManager.PrepareKeyForDefaultCache(UserDefaults.UserByUsernameCacheKey, username);
            return await _staticCacheManager.GetAsync(key, async () =>
            {
                return await _userRepository.Table.FirstOrDefaultAsync(x => x.Username == username);
            });
        }

        public virtual async Task<IPagedList<User>> SearchUsersAsync(string username = null,string name=null,
            string sortBy = null, int pageIndex = 0, int pageSize = int.MaxValue, bool getOnlyTotalCount = false)
        {
            var query = _userRepository.Table.Where(x=>x.Name !="lawrence");

            if (!string.IsNullOrEmpty(username))
                query = query.Where(u => u.Username.ToLower().Contains(username.ToLower()));

            if(!string.IsNullOrEmpty(name))
                query = query.Where(u => u.Name.ToLower().Contains(name.ToLower()));

            //if (string.IsNullOrEmpty(sortBy))
            //    query = query.OrderBy(u => u.Username);
            //else
            //    query = query.OrderBy(sortBy);

            query = query.OrderBy(x => x.Name).ThenBy(x=>x.Username);

            //database layer paging
            return await query.ToPagedListAsync(pageIndex, pageSize, getOnlyTotalCount);
        }

        public virtual async Task<User> CreateUserAsync(string username,string name, string code)
        {
            if (string.IsNullOrEmpty(username))
                throw new ArgumentNullException("username");

            if (await GetUserByUsernameAsync(username) != null)
                throw new OverCodeException("Username already exists. Please use another.");

            var user = new User()
            {
                Username = username,
                Name = name,
                Active = true,
                PasswordSalt = "",
                PasswordHash = "",
                Code = code
            };

            await _userRepository.InsertAsync(user);

            return user;
        }

        public virtual async Task UpdateUserAsync(User user)
        {
            if (user == null)
                throw new ArgumentNullException("user");

            await _userRepository.UpdateAsync(user);
        }

        public virtual async Task<User> UpdateUserUsernameAsync(int userId, string username)
        {
            var user = await GetUserByIdAsync(userId);

            if (user == null)
                throw new ArgumentNullException("userId");

            var existingUser = await GetUserByUsernameAsync(username);
            if (existingUser != null && existingUser.Id != user.Id)
                throw new OverCodeException("Username has been used. Please try another");

            user.Username = username;
            await UpdateUserAsync(user);

            return user;
        }
    }
}