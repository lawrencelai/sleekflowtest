﻿using OverCode.Core.Caching;
using OverCode.Core.Events;
using SleekFlow.Users.Dtos;
using System.Threading.Tasks;

namespace SleekFlow.Users.Services.Cache
{
    public partial class UserCacheEventConsumer : CacheEventConsumer<User>
    {
        public UserCacheEventConsumer(IStaticCacheManager staticCacheManager) : base(staticCacheManager)
        {
        }

        protected override async Task ClearCacheAsync(User entity)
        {
            await RemoveAsync(UserDefaults.UserByUsernameCacheKey, entity.Username);
        }
    }
}