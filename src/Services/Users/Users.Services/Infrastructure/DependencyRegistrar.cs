﻿using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using OverCode.Core.Infrastructure;
using OverCode.Core.Infrastructure.DependencyManagement;
using SleekFlow.Users.Services;
using Users.Services.Interface;

namespace SleekFlow.Users.Services.Infrastructure
{
    public class DependencyRegistrar : IDependencyRegistrar
    {
        /// <summary>
        /// Register services and interfaces
        /// </summary>
        /// <param name="services">Collection of service descriptors</param>
        /// <param name="typeFinder">Type finder</param>
        /// <param name="configuration">Configuration</param>
        public virtual void Register(IServiceCollection services, ITypeFinder typeFinder, IConfiguration configuration)
        {
            services.AddScoped<IUserService,UserService>();
            services.AddScoped<IUserPasswordService, UserPasswordService>();
        }

        /// <summary>
        /// Order of this dependency registrar implementation
        /// </summary>
        public int Order => 10;
    }
}