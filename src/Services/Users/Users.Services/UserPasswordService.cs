﻿using SleekFlow.Users.Dtos;
using OverCode.Core;
using OverCode.Core.Cryptography;
using System.Text;
using System.Threading.Tasks;
using Users.Services.Interface;

namespace SleekFlow.Users.Services
{
    public class UserPasswordService : IUserPasswordService
    {
        #region Fields

        private readonly IUserService _userService;

        #endregion

        #region Ctor

        public UserPasswordService(IUserService userService)
        {
            _userService = userService;
        }

        #endregion

        public async Task ValidatePasswordAsync(string password)
        {
            if (string.IsNullOrEmpty(password))
                throw new OverCodeException("Password cannot be empty");


            if (password.Length < 8)
                throw new OverCodeException("Password cannot be empty");
        }

        public virtual async Task SetPasswordAsync(User user, string password)
        {
            await ValidatePasswordAsync(password);

            user.PasswordSalt = Hash.CreateSaltKey(5);
            user.PasswordHash = Hash.GetHash(Encoding.UTF8, password, HashType.SHA512, user.PasswordSalt);

            await _userService.UpdateUserAsync(user);
        }

        public virtual async Task<bool> PasswordMatchAsync(string passwordHash, string passwordSalt, string enteredPassword)
        {
            if (passwordHash == null || string.IsNullOrEmpty(enteredPassword))
                return false;

            return await Task.FromResult(Hash.CheckHash(Encoding.UTF8, enteredPassword, passwordHash, HashType.SHA512, passwordSalt));
        }

        public virtual async Task ChangePasswordWithCurrentPasswordAsync(User user, string currentPassword, string newPassword)
        {
            await ValidatePasswordAsync(newPassword);

            if (!await PasswordMatchAsync(user.PasswordHash, user.PasswordSalt, currentPassword))
                throw new OverCodeException("Invalid current password");

            await SetPasswordAsync(user, newPassword);
        }
    }
}