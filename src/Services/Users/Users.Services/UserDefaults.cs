﻿using OverCode.Core.Caching;

namespace SleekFlow.Users.Services
{
    public class UserDefaults
    {
        public static CacheKey UserByUsernameCacheKey => new CacheKey("User.by.username.{0}");

    }
}
