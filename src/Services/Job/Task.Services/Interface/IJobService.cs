﻿using Job.Dtos;
using Job.Dtos.Enum;
using OverCode.Core;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Job.Services.Interface
{
    public interface IJobService
    {
        Task<Dtos.Job> CreateJob(CreateJobDto request);

        Task UpdateJobAsync(Dtos.Job Job);

        Task<Dtos.Job> GetJobByIdAsync(int id);

        Task<IPagedList<Dtos.Job>> SearchJobAsync(
            int pageIndex = 0, int pageSize = int.MaxValue, bool getOnlyTotalCount = false,
             DateTime? createdDateFrom = null, DateTime? createdDateTo = null,
             List<int> status = null, int? createdBy = null, string sortBy = null);

        Task DeleteJobAsync(int id);
    }
}
