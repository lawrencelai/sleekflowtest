﻿using OverCode.Core;
using OverCode.LinqToDb.Core;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Dynamic.Core;
using Job.Dtos;
using System.Threading.Tasks;
using Job.Dtos.Enum;
using Job.Services.Interface;

namespace Job.Services
{
    public class JobService : IJobService
    {
        #region Fields
        private readonly IRepository<Dtos.Job> _jobRepository;
        #endregion

        #region Ctor

        public JobService(
             IRepository<Dtos.Job> jobRepository
            )
        {
            _jobRepository = jobRepository;
        }

        #endregion


        public virtual async Task<Dtos.Job> CreateJob(CreateJobDto request)
        {
            var Job = new Dtos.Job()
            {
                Id = request.Id,
                Name = request.Name,
                Description = request.Description,
                DueDateUtc = request.DueDateUtc,
                Status = request.Status,
                CreatedBy = request.CreatedBy,
                CreatedDateTimeUtc = DateTime.UtcNow,
            };

            await _jobRepository.InsertAsync(Job);

            return Job;
        }

        public virtual async Task UpdateJobAsync(Dtos.Job Job)
        {
            if (Job == null)
                throw new ArgumentNullException("Job is null");

            await _jobRepository.UpdateAsync(Job);
        }

        public virtual async Task<Dtos.Job> GetJobByIdAsync(int id)
        {
            return await _jobRepository.Table.FirstOrDefaultAsync(x => x.Id == id);
        }

        public virtual async Task<IPagedList<Dtos.Job>> SearchJobAsync(
            int pageIndex = 0, int pageSize = int.MaxValue, bool getOnlyTotalCount = false,
             DateTime? createdDateFrom = null, DateTime? createdDateTo = null,
             List<int> status = null, int? createdBy=null, string sortBy = null)
        {
            var query = _jobRepository.Table;

            if (status != null)
                if (status.Count > 0)
                    query = query.Where((x) => status.Contains(x.Status));

            if (createdDateFrom.HasValue)
                query = query.Where(x => createdDateFrom.Value.Date <= x.CreatedDateTimeUtc.Date);

            if (createdDateTo.HasValue)
                query = query.Where(x => createdDateTo.Value.Date >= x.CreatedDateTimeUtc.Date);

            if (createdBy.HasValue)
                    query = query.Where(x => x.CreatedBy == createdBy);

            if (string.IsNullOrEmpty(sortBy))
                query = query.OrderByDescending(x => x.CreatedDateTimeUtc);
            else
                query = query.OrderBy(sortBy);


            //database layer paging
            return await query.ToPagedListAsync(pageIndex, pageSize, getOnlyTotalCount);
        }

        public virtual async Task DeleteJobAsync(int id)
        {
            var job = await GetJobByIdAsync(id);

            if (job == null)
                throw new ArgumentNullException("job is null");

            await _jobRepository.DeleteAsync(job);
        }
    }
}