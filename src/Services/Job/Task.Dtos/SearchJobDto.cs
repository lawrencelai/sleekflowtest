﻿using Job.Dtos.Enum;
using System.Collections.Generic;

namespace Job.Dtos
{
    public class SearchJobDto
    {
        public string StartDate { get; set; }

        public string EndDate { get; set; }

        public string SortBy { get; set; }

        public List<int> Status { get; set; }

        public int? CreatedBy { get; set; }
    }
}
