﻿using Job.Dtos.Enum;
using OverCode.Core;
using System;

namespace Job.Dtos
{
    public class Job : BaseEntity
    {
        public string Name { get; set; }

        public string Description { get; set; }

        public DateTime? DueDateUtc { get; set; }

        public int Status { get; set; }

        public int CreatedBy { get; set; }

        public DateTime CreatedDateTimeUtc { get; set; }

        public DateTime? CompletedDateTimeUtc { get; set; }
    }
}