﻿using Job.Dtos.Enum;
using System;

namespace Job.Dtos
{
    public class CreateJobDto
    {
        public int Id { get; set; }

        public string Name { get; set; }

        public string Description { get; set; }


        public DateTime? DueDateUtc { get; set; }

        public int Status { get; set; }

        public int CreatedBy { get; set; }

        public DateTime? CompletedDateTimeUtc { get; set; }
    }
}
