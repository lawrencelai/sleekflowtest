﻿namespace Job.Dtos.Enum
{
    public enum JobStatus
    {
        NotStarted,
        InProgress,
        Completed
    }
}
